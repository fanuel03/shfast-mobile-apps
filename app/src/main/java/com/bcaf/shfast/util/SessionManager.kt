package com.bcaf.shfast.util

import android.content.Context
import android.content.SharedPreferences
import com.bcaf.shfast.persistence.response.showroom.RekeningShowroom
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.lang.reflect.Type


object SessionManager {
    private const val SP_NAME = "SHFAST"
    private const val ROLE = "ROLE"
    private const val ID = "ID"
    private const val TOKEN = "TOKENSHF"
    private const val WILAYAH = "wilayah"
    private const val UNAME = "USERNAMEISHF"
    private const val FULLNAME = "fullName"
    private const val NOREK = "norek"

    private const val NIK = "NIK"
    private const val TANGGAL_EXPIRED = "TANGGAL_EXPIRED"

    val PASS = "PASSWORDISHF"
    val REMEMBER = "REMEMBERISHF"
    val LOGIN_FLAG = "LOGIN_FLAGISHF"
    val FCM_TOKEN = "FCM_TOKENISHF"

    fun flush(context: Context) {
        val editor: SharedPreferences.Editor = retrieve(context)!!.edit()
        editor.clear()
        editor.apply()
    }

    private fun retrieve(context: Context): SharedPreferences? {
        return context.getSharedPreferences(
            SP_NAME,
            Context.MODE_PRIVATE
        )
    }

    fun saveLoginData(
        context: Context,
        uname: String?,
        token: String?,
        id: String?,
        role: String?,
        fullname: String?,
        wilayah: String?
    ) {
        val editor: SharedPreferences.Editor = retrieve(context)!!.edit()
        editor.putString(UNAME, uname)
        editor.putString(TOKEN, token)
        editor.putString(ID, id)
        editor.putString(ROLE, role)
        editor.putString(WILAYAH, wilayah)
        editor.putString(FULLNAME, fullname)
        editor.apply()
        editor.commit()
    }

    fun setNIK(context: Context, nik: String) {
        retrieve(context)!!.edit().apply {
            this.putString(NIK, nik)
            this.apply()
        }
    }

    fun saveNorek(context: Context, listNorek: List<RekeningShowroom>) {
        val json = Gson().toJson(listNorek)
        val editor: SharedPreferences.Editor = retrieve(context)!!.edit()
        editor.putString(NOREK, json)
        editor.apply()
        editor.commit()
    }

    fun setTanggalExpired(context: Context, tanggal: String) {
        retrieve(context)!!.edit().apply {
            this.putString(TANGGAL_EXPIRED, tanggal)
            this.apply()
        }
    }

    fun getUsername(context: Context) = retrieve(context)?.getString(UNAME, "")
    fun getNik(context: Context) = retrieve(context)?.getString(NIK, "")
    fun getNama(context: Context) = retrieve(context)?.getString(FULLNAME, "")
    fun getRole(context: Context) = retrieve(context)?.getString(ROLE, "")
    fun getId(context: Context) = retrieve(context)?.getString(ID, "")
    fun getWilayah(context: Context)= retrieve(context)?.getString(WILAYAH, "")
    fun getToken(context: Context) = retrieve(context)?.getString(TOKEN, "")
    fun getTanggalExpired(context: Context) = retrieve(context)?.getString(TANGGAL_EXPIRED, "")
    fun getNorek(context: Context): ArrayList<RekeningShowroom> {
        val str = context.getSharedPreferences(SP_NAME, Context.MODE_PRIVATE).getString(NOREK, null) ?: ""
        val type: Type = object : TypeToken<List<RekeningShowroom?>?>() {}.type
        return Gson().fromJson(str, type)
    }
}