package com.bcaf.shfast.util

object SHFConstant {
    const val STEP_REQUEST = "Harga Mobil yang diajukan\t:"
    const val STEP_APPROVED = "Harga Mobil yang disetujui\t:"
    const val STEP_REJECTED = "Harga Mobil yang ditolak\t:"

    const val ROLE_USER = "ROLE_USER"
    const val ROLE_CMO = "ROLE_CMO"
    const val ROLE_KOMITE = "ROLE_KOMITE"

    const val MENU_DASHBOARD = "MENU_DASHBOARD"
    const val MENU_HISTORY = "MENU_HISTORY"
    const val MENU_SIMULATION = "MENU_SIMULATION"
    const val MENU_SETTING = "MENU_SETITNG"

    const val HTTP_CODE_403 = "Token is expired/invalid"
}