package com.bcaf.shfast.util

object DateUtil {

    val bulan: MutableMap<String, Int> = hashMapOf(
        "Jan" to 0,
        "Feb" to 1,
        "Mar" to 2,
        "Apr" to 3,
        "May" to 4,
        "Jun" to 5,
        "Jul" to 6,
        "Aug" to 7,
        "Sep" to 8,
        "Oct" to 9,
        "Nov" to 10,
        "Dec" to 11
    )
    

    fun extractDate(tanggal: String): ArrayList<Int> {
        val arrTanggal = tanggal.split(" ")
        return arrayListOf(
            arrTanggal[0].toInt(),
            bulan[arrTanggal[1]]!!,
            arrTanggal[2].toInt()
        )
    }
}