package com.bcaf.shfast.util

import android.util.ArrayMap
import com.bcaf.shfast.persistence.model.Dokumen
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import org.json.JSONArray
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap

object RequestUtil {

    //Login
    private const val KEY_USERNAME = "username"
    private const val KEY_PASSWORD = "password"

    //Pengajuan SHF
    private const val KEY_SHF = "id_shf"
    private const val KEY_OTR = "otr"
    private const val KEY_NOPLAT = "noPlat"
    private const val KEY_BPKB = "url_bpkb"
    private const val KEY_STNK = "url_stnk"
    private const val KEY_FAKTUR = "url_faktur"
    private const val KEY_FOKEN = "url_foken_d"
    private const val KEY_FOKENB = "url_foken_b"
    private const val KEY_FOKEN_Ka = "url_foken_ka"
    private const val KEY_FOKEN_Ki = "url_foken_ki"
    private const val KEY_ACC_NO = "accno"

    private const val KEY_IDSHOWROOM = "idShowroom"


    private fun map2JSON(map: Map<String, Any?>): RequestBody {
        return JSONObject(map).toString()
            .toRequestBody("application/json; charset=utf-8".toMediaType())
    }

    private fun ArrayofMap2JSON(map: ArrayList<Map<String, Any>?>): RequestBody {
        return JSONArray(map).toString()
            .toRequestBody("application/json; charset=utf-8".toMediaType())
    }

    //untuk Login
    fun createLoginRequest(
        username: String?,
        password: String?
    ): RequestBody {
        val map: MutableMap<String, Any?> =
            ArrayMap()
        map[KEY_PASSWORD] = password
        map[KEY_USERNAME] = username
        return map2JSON(map)
    }

    fun createLdapRequest(
        userid: String,
        pass: String
    ): RequestBody {
        val child: MutableMap<String, String> =
            HashMap()
        child["UserId"] = userid
        child["Password"] = pass
        val map: MutableMap<String, Any?> =
            HashMap()
        map["Credentials"] = child
        return map2JSON(map)
    }

    //untuk Penggunaan Plafond
    fun createPengajuanRequest(
        uuid: String,
        nopol: String,
        otr: Int,
        idShowroom: Int,
        dokumenList: ArrayList<Dokumen>,
        noAkun: String
    ): RequestBody? {
        val request = ArrayList<Map<String, Any>>()
        val resMap = hashMapOf<String, Any>().apply {
            this[KEY_NOPLAT] = nopol
            this[KEY_OTR] = otr
            this[KEY_SHF] = uuid
            this[KEY_BPKB] = dokumenList.find { it.paramName == "BPKB" }!!.downloadUri!!
            this[KEY_STNK] = dokumenList.find { it.paramName == "STNK" }!!.downloadUri!!
            this[KEY_FAKTUR] = dokumenList.find { it.paramName == "FAKTUR" }!!.downloadUri!!
            this[KEY_FOKEN] = dokumenList.find { it.paramName == "FOKEN_D" }!!.downloadUri!!
            this[KEY_FOKENB] = "/gambarSHF/NotUse.jpg"
            this[KEY_FOKEN_Ki] = "/gambarSHF/NotUse.jpg"
            this[KEY_FOKEN_Ka] = "/gambarSHF/NotUse.jpg"
            this[KEY_ACC_NO] = noAkun
        }

        request.add(resMap)

        val map: MutableMap<String, Any?> = HashMap()
        map[KEY_IDSHOWROOM] = idShowroom
        map["pengajuanSHF"] = request
        return map2JSON(map)
    }

    fun createCancelRequest(idPengajuan: String): RequestBody? {
        val map: MutableMap<String, Any> = HashMap()
        map["idPenggunaan"] = idPengajuan.toInt()
        return map2JSON(map)
    }

    fun createListKontrakRequest(nik: String): RequestBody {
        val child: MutableMap<String, String> = HashMap()
        child["NIK"] = nik
        val map: MutableMap<String, Any> = HashMap()
        map["RequestParameter"] = child
        map["TrxId"] = "string"
        map["Source"] = "test"
        return map2JSON(map)
    }

    private fun changeFormat(`in`: String): String {
        val tmp = Calendar.getInstance()
        //langkah pertama
        var sdf = SimpleDateFormat("dd MMM yyyy")
        tmp.time = sdf.parse(`in`)
        //langkah kedua
        sdf = SimpleDateFormat("yyyy-MM-dd")
        return sdf.format(tmp.time)

    }

    fun createSimulasiRequest(norek: String, nopin: String, tglEt: String, ambilBPKB: String): RequestBody? {
        val child: MutableMap<String, String> = HashMap()
        child["TanggalET"] = changeFormat(tglEt)
        child["NoRek"] = norek
        child["NoPin"] = nopin
        child["AmbilBPKB"] = ambilBPKB
        val map: MutableMap<String, Any> = HashMap()
        map["ETSHFastRequest"] = child
        map["Source"] = "SHFast"
        return map2JSON(map)
    }

    fun createLogoutRequest(username: String): RequestBody {
        val map: MutableMap<String, Any> = HashMap()
        map["email"] = username
        return map2JSON(map)
    }

    fun createApprovalRequest(idPenggunaan: String, otr: String, reason: String, status: String): RequestBody? {
        val map: MutableMap<String, Any> = HashMap()
        map["idPenggunaan"] = idPenggunaan.toInt()
        map["otrRekomen"] = otr
        map["kondisi"] = reason
        map["status"] = status
        return map2JSON(map)
    }

    fun createRejectRequest(idPenggunaan: String, reason: String, idShowroom: String): RequestBody? {
        val map: MutableMap<String, Any> = HashMap()
        map["idPenggunaan"] = idPenggunaan.toInt()
        map["idShowroom"] = idShowroom.toInt()
        map["keteranganReject"] = reason

        return map2JSON(map)
    }

    fun createDetaiPenggunaanRequest(idPenggunaan: String): RequestBody? {
        val map: MutableMap<String, Any> = HashMap()
        map["idPenggunaan"] = idPenggunaan.toInt()
        return map2JSON(map)
    }

    fun createChangePassword(oldPassword: String, newPassword: String): RequestBody? {
        val map: MutableMap<String, Any> = HashMap()
        map["oldPassword"] = oldPassword
        map["newPassword"] = newPassword
        map["confirmPassword"] = newPassword
        return map2JSON(map)

    }
}