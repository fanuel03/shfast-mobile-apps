package com.bcaf.shfast.util

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.widget.Toast
import com.bcaf.shfast.ext.toRupiahWithoutCurrency
import com.bcaf.shfast.persistence.model.Dokumen
import com.bcaf.shfast.ui.LoginActivity
import java.math.BigDecimal
import java.text.SimpleDateFormat
import java.util.*

object SHFUtil {
    fun tokenExpired(ctx: Context) {

        Toast.makeText(ctx, "Sesi anda telah habis, silahkan login ulang", Toast.LENGTH_LONG).show()
        SessionManager.flush(ctx)
        // set the new task and clear flags
        ctx.startActivity(LoginActivity.getIntent(ctx).apply {
            flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        })

    }

    fun isEmailValid(s: String): Boolean {
        val tmp = s.toCharArray()
        var result = true
        for (i in tmp.indices) {
            result = result &&
                    (tmp[i].toByte().toInt() in 48..57
                            || tmp[i].toByte().toInt() in 65..90
                            || tmp[i].toByte().toInt() in 97..122
                            || tmp[i] == '@' || tmp[i] == '.' || tmp[i] == '_')
        }
        return result
    }

    @SuppressLint("SimpleDateFormat")
    fun parseTime(date: String): String? {
        val tmp = Calendar.getInstance()
        return try {
            val sdf = SimpleDateFormat("yyyy-MM-dd HH:mm:ss.s")
            tmp.time = sdf.parse(date)
            formatTime(tmp)
        } catch (e: Exception) {
            try {
                val sdf = SimpleDateFormat("yyyy-MM-dd")
                tmp.time = sdf.parse(date)
                formatTime(tmp)
            } catch (f: Exception) {
                try {
                    val sdf = SimpleDateFormat("yyyy/MM/dd")
                    tmp.time = sdf.parse(date)
                    formatTime(tmp)
                } catch (g: Exception) {
                    try {
                        val sdf =
                            SimpleDateFormat("dd/MM/yyyy")
                        tmp.time = sdf.parse(date)
                        formatTime(tmp)
                    } catch (h: Exception) {
                        "-"
                    }
                }
            }
        }
    }

    fun parseTrackingTime(date: String): String? {
        val tmp = Calendar.getInstance()
        return try {
            val sdf = SimpleDateFormat("dd/MM/yyyy")
            tmp.time = sdf.parse(date)
            formatTime(tmp)
        } catch (e: Exception) {
            date
        }
    }

    fun formatTime(date: Calendar): String? {
        //konversi ke string
        val formatter = SimpleDateFormat("dd/MM/yyyy")
        return formatter.format(date.time)
    }

    fun getDokumenPersyaratan(): ArrayList<Dokumen> {
        return arrayListOf<Dokumen>(
            Dokumen("BPKB", "BPKB", "", "BPKB"),
            Dokumen("Faktur Pembelian Mobil", "Faktur", "", "FAKTUR"),
            Dokumen("STNK", "STNK", "", "STNK"),
            Dokumen("Tampak Depan (Terlihat Plat Nomor)", "Foto Depan", "", "FOKEN_D")
        )
    }

    fun rupiahFormat(s: BigDecimal): String {
        return if (s.toString().length < 19) {
            var value = s.toString()
                .replace(".", "")
            if (value == "") {
                value = "0"
            }
            val result: String = value.toLong().toRupiahWithoutCurrency()
            result
        } else {
            s.toString().substring(0, 18)
        }

    }

    fun buildUrlOLX(merk: String, tipe: String): String {
        return "https://www.olx.co.id/items/q-" +
                merk.replace(" ", "-") +
                "-" +
                tipe.replace(" ", "-")
    }

    fun buildUrlCarmudi(merk: String, tipe: String): String {
        return "https://www.carmudi.co.id/cars/used/q:" +
                merk.replace(" ", "+") +
                "+" +
                tipe.replace(" ", "+") +
                "/"
    }

}