package com.bcaf.shfast.util

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import java.io.ByteArrayOutputStream
import java.io.File

object FileUtil {
    const val MAXSIZE = 4 * 8 * 1024 * 1024 * 1024.0 //satuan Byte (1.5 MB)

    private fun imageToByte(path: String?, quality: Int): ByteArray {
        val options = BitmapFactory.Options()
        val bitmap = BitmapFactory.decodeFile(path, options)
        val outputStream = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.JPEG, quality, outputStream)
        return outputStream.toByteArray()
    }

    fun compress(path: String): ByteArray {
        val compress: Double
        compress = try {
            val tmp = File(path)
            val size: Long = tmp.usableSpace
            if (size > MAXSIZE) 50.0 else 100.0
        } catch (e: java.lang.Exception) {
            100.0
        }
        return imageToByte(path, compress.toInt())
    }
}