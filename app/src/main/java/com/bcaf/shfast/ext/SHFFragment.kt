package com.bcaf.shfast.ext

import androidx.fragment.app.Fragment

abstract class SHFFragment: Fragment() {
   abstract fun refreshData()
}