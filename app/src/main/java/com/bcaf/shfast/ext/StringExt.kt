package com.bcaf.shfast.ext

import java.math.RoundingMode
import java.text.NumberFormat
import java.util.*


fun String?.toRupiah(): String {
    if (this == null) return "Rp. 0"
    val formatter = NumberFormat.getNumberInstance(Locale.US)
    val n = this.toBigDecimal().setScale(4, RoundingMode.HALF_EVEN)

    val hasil = formatter.format(n)
        .replace(".",";")
        .replace(",", ".")
        .replace(";", ",")

    return "Rp. $hasil"
}