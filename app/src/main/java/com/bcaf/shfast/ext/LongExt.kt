package com.bcaf.shfast.ext

import java.text.NumberFormat
import java.util.*

fun Long.toRupiah(): String {
    var hasil: String
    val n = NumberFormat.getNumberInstance(Locale.US)
    hasil = n.format(this)
    hasil = hasil.replace(",", ".")
    hasil = "Rp.$hasil"
    return hasil
}

fun Long.toRupiahWithoutCurrency(): String {
    var hasil: String
    val n = NumberFormat.getNumberInstance(Locale.US)
    hasil = n.format(this)
    hasil = hasil.replace(",", ".")
    return hasil
}