package com.bcaf.shfast.ext

import java.math.BigDecimal
import java.math.RoundingMode

fun BigDecimal?.toRupiah(): String {
    if (this == null) return "Rp. 0"
    val n = this.setScale(2, RoundingMode.HALF_EVEN)
    return "Rp. $n"
}