package com.bcaf.shfast.ext

fun Boolean.toInt() = if (this) 1 else 0
fun Boolean.toIntString() = if (this) "1" else "0"
