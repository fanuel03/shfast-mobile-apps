package com.bcaf.shfast.ext

import android.os.Handler
import android.os.Looper
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody
import okio.BufferedSink
import java.io.File
import java.io.FileInputStream
import java.io.IOException


data class FileUploadRequestBody (
    val file: File? = null,
    val path: String? =  null,
    val listener: FileUploadCallback? = null,
    val type: String? = null

): RequestBody() {

    override fun contentType() = ("$type/*").toMediaTypeOrNull()

    override fun contentLength() = file?.length() ?: 0

    @Throws(IOException::class)
    override fun writeTo(sink: BufferedSink) {
        val fileLength: Long = file!!.length()
        val buffer = ByteArray(DEFAULT_BUFFER_SIZE)
        val `in` = FileInputStream(file)
        var uploaded: Long = 0
        try {
            var read: Int
            val handler = Handler(Looper.getMainLooper())
            while (`in`.read(buffer).also { read = it } != -1) {

                // update progress on UI thread
                handler.post(ProgressUpdater(uploaded, fileLength, listener!!))
                uploaded += read.toLong()
                sink.write(buffer, 0, read)
            }
        } finally {
            `in`.close()
        }
    }

    class ProgressUpdater(private val mUploaded: Long, private val mTotal: Long, private val listener: FileUploadCallback) :
        Runnable {
        override fun run() {
            listener.onUpdate((100 * mUploaded / mTotal).toInt())
        }

    }


}

interface FileUploadCallback {
    fun onUpdate(percentage: Int)
}