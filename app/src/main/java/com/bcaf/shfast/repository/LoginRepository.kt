package com.bcaf.shfast.repository

import com.bcaf.shfast.data.ApiHelper
import com.bcaf.shfast.persistence.api.Resource
import com.bcaf.shfast.persistence.api.ResponseHandler
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import okhttp3.RequestBody

class LoginRepository(
    private val apiHelper: ApiHelper,
    private val responseHandler: ResponseHandler
) {
    suspend fun login(loginRequest: RequestBody): Resource<Any> {
        return withContext(Dispatchers.IO) {
            try {
                responseHandler.handleSuccess( apiHelper.login(loginRequest))
            } catch (e: Exception) {
                responseHandler.handleException<Exception>(e)
            }
        }
    }

    suspend fun loginLdap(ldapRequestBody: RequestBody): Resource<Any> {
        return withContext(Dispatchers.IO) {
            try {
                responseHandler.handleSuccess( apiHelper.loginLdap(ldapRequestBody))
            } catch (e: Exception) {
                responseHandler.handleException<Exception>(e)
            }
        }
    }

    suspend fun getShowroomDashboard(token: String): Resource<Any> {
        return withContext(Dispatchers.IO) {
            try {
                responseHandler.handleSuccess( apiHelper.getDashboard(token))
            } catch (e: Exception) {
                responseHandler.handleException<Exception>(e)
            }
        }
    }
}