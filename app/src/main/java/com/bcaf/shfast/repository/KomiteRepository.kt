package com.bcaf.shfast.repository

import com.bcaf.shfast.data.komite.KomiteHelper
import com.bcaf.shfast.persistence.api.Resource
import com.bcaf.shfast.persistence.api.ResponseHandler
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import okhttp3.RequestBody

class KomiteRepository(
    private val komiteHelper: KomiteHelper,
    private val responseHandler: ResponseHandler
) {
    suspend fun getBucketKomite(token: String): Resource<Any> {
        return withContext(Dispatchers.IO) {
            try {
                responseHandler.handleSuccess( komiteHelper.getBucketKomite(token))
            } catch (e: Exception) {
                responseHandler.handleException<Exception>(e)
            }
        }
    }

    suspend fun getDetailBucket(token: String, body: RequestBody): Resource<Any> {
            return withContext(Dispatchers.IO) {
                try {
                    responseHandler.handleSuccess( komiteHelper.getDetailBucket(token, body))
                } catch (e: Exception) {
                    responseHandler.handleException<Exception>(e)
                }
            }
    }

    suspend fun putApproval(token: String, body: RequestBody): Resource<Any> {
        return withContext(Dispatchers.IO) {
            try {
                responseHandler.handleSuccess( komiteHelper.putApproval(token, body))
            } catch (e: Exception) {
                responseHandler.handleException<Exception>(e)
            }
        }
    }

    suspend fun putReject(token: String, body: RequestBody): Resource<Any> {
        return withContext(Dispatchers.IO) {
            try {
                responseHandler.handleSuccess( komiteHelper.putReject(token, body))
            } catch (e: Exception) {
                responseHandler.handleException<Exception>(e)
            }
        }
    }

    suspend fun history(token: String, status: String, sort: String): Resource<Any> {
        return withContext(Dispatchers.IO) {
            try {
                responseHandler.handleSuccess( komiteHelper.history(token, status, sort))
            } catch (e: Exception) {
                responseHandler.handleException<Exception>(e)
            }
        }
    }

    suspend fun getDetailHistory(token: String, body: RequestBody): Resource<Any> {
        return withContext(Dispatchers.IO) {
            try {
                responseHandler.handleSuccess( komiteHelper.getDetailHistory(token, body))
            } catch (e: Exception) {
                responseHandler.handleException<Exception>(e)
            }
        }
    }

    suspend fun logout(token: String, body: RequestBody): Resource<Any> {
        return withContext(Dispatchers.IO) {
            try {
                responseHandler.handleSuccess(komiteHelper.logout(token, body))
            } catch (e: Exception) {
                responseHandler.handleException<Exception>(e)
            }
        }
    }
}