package com.bcaf.shfast.repository

import com.bcaf.shfast.data.ApiHelper
import com.bcaf.shfast.persistence.api.Resource
import com.bcaf.shfast.persistence.api.ResponseHandler
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class SplashScreenRepository(
    private val apiHelper: ApiHelper,
    private val responseHandler: ResponseHandler) {

    suspend fun getShowroomDashboard(token: String): Resource<Any> {
        return withContext(Dispatchers.IO) {
            try {
                responseHandler.handleSuccess( apiHelper.getDashboard(token))
            } catch (e: Exception) {
                responseHandler.handleException<Exception>(e)
            }
        }
    }

    suspend fun getBucketKomite(token: String): Resource<Any> {
        return withContext(Dispatchers.IO) {
            try {
                responseHandler.handleSuccess(apiHelper.getBucketKomite(token))
            } catch (e: Exception) {
                responseHandler.handleException<Exception>(e)
            }
        }
    }
}