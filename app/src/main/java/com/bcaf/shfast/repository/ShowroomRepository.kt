package com.bcaf.shfast.repository

import com.bcaf.shfast.data.showroom.ShowroomHelper
import com.bcaf.shfast.persistence.api.Resource
import com.bcaf.shfast.persistence.api.ResponseHandler
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import okhttp3.MultipartBody
import okhttp3.RequestBody

class ShowroomRepository(
    private val showroomHelper: ShowroomHelper,
    private val responseHandler: ResponseHandler
) {
    suspend fun getShowroomDashboard(token: String): Resource<Any> {
        return withContext(Dispatchers.IO) {
            try {
                responseHandler.handleSuccess( showroomHelper.getDashboard(token))
            } catch (e: Exception) {
                responseHandler.handleException<Exception>(e)
            }
        }
    }

    suspend fun generateUuid(token: String): Resource<Any> {
        return withContext(Dispatchers.IO) {

            try {
                responseHandler.handleSuccess(showroomHelper.generateUuid(token))
            } catch (e: Exception) {
                responseHandler.handleException<Exception>(e)
            }
        }
    }

    suspend fun uploadFile(token: String, img: MultipartBody.Part): Resource<Any> {
        return withContext(Dispatchers.IO) {
            try {
                responseHandler.handleSuccess(showroomHelper.uploadFile(token, img))
            } catch (e: Exception) {
                responseHandler.handleException<Exception>(e)
            }
        }
    }

    suspend fun pengajuan(token: String, requestBody: RequestBody): Resource<Any> {
        return withContext(Dispatchers.IO) {
            try {
                responseHandler.handleSuccess(showroomHelper.pengajuan(token, requestBody))
            } catch (e: Exception) {
                responseHandler.handleException<Exception>(e)
            }
        }
    }

    suspend fun history(token: String, status: String, sort: String): Resource<Any> {
        return withContext(Dispatchers.IO) {
            try {
                responseHandler.handleSuccess(showroomHelper.history(token, status, sort))
            } catch (e: Exception) {
                responseHandler.handleException<Exception>(e)
            }
        }
    }

    suspend fun detail(token: String, requestBody: RequestBody): Resource<Any>{
        return withContext(Dispatchers.IO) {
            try {
                responseHandler.handleSuccess(showroomHelper.detail(token, requestBody))
            } catch (e: Exception) {
                responseHandler.handleException<Exception>(e)
            }
        }
    }

    suspend fun cancel(token: String, body: RequestBody): Resource<Any> {
        return withContext(Dispatchers.IO) {
            try {
                responseHandler.handleSuccess(showroomHelper.cancel(token, body))
            } catch (e: Exception) {
                responseHandler.handleException<Exception>(e)
            }
        }
    }

    suspend fun getListKontrak(token: String, body: RequestBody): Resource<Any> {
        return withContext(Dispatchers.IO) {
            try {
                responseHandler.handleSuccess(showroomHelper.getListKontrak(token, body))
            } catch (e: Exception) {
                responseHandler.handleException<Exception>(e)
            }
        }
    }

    suspend fun getSimulasi(token: String, body: RequestBody): Resource<Any> {
        return withContext(Dispatchers.IO) {
            try {
                responseHandler.handleSuccess(showroomHelper.getSimulasi(token, body))
            } catch (e: Exception) {
                responseHandler.handleException<Exception>(e)
            }
        }
    }

    suspend fun logout(token: String, body: RequestBody): Resource<Any> {
        return withContext(Dispatchers.IO) {
            try {
                responseHandler.handleSuccess(showroomHelper.logoout(token, body))
            } catch (e: Exception) {
                responseHandler.handleException<Exception>(e)
            }
        }
    }

    suspend fun changePassword(token: String, body: RequestBody): Resource<Any> {
        return withContext(Dispatchers.IO) {
            try {
                responseHandler.handleSuccess(showroomHelper.logoout(token, body))
            } catch (e: Exception) {
                responseHandler.handleException<Exception>(e)
            }
        }
    }
}