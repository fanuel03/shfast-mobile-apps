package com.bcaf.shfast.data.komite

import okhttp3.RequestBody

class KomiteHelperImpl(private val service: KomiteService): KomiteHelper {
    override suspend fun getBucketKomite(token: String?) =
        service.getBucketKomite(token)

    override suspend fun getDetailBucket(token: String?, body: RequestBody?) = service.getDetailBucket(token, body)

    override suspend fun putApproval(token: String?, body: RequestBody?) = service.putApproval(token, body)
    override suspend fun putReject(token: String?, body: RequestBody?) = service.putReject(token, body)

    override suspend fun history(
        token: String?,
        status: String?,
        sort: String?
    ) = service.history(token, status, sort)

    override suspend fun getDetailHistory(token: String?, body: RequestBody?) = service.getDetailHistory(token, body)

    override suspend fun logout(token: String?, body: RequestBody?) = service.logout(token, body)
}