package com.bcaf.shfast.data

import com.bcaf.shfast.persistence.response.credential.LdapBody
import com.bcaf.shfast.persistence.response.credential.LoginResponseBody
import com.bcaf.shfast.persistence.response.dashboard.BucketKomiteBody
import com.bcaf.shfast.persistence.response.showroom.InfoShowroomBody
import com.bcaf.shfast.persistence.response.showroom.ShowroomDashboardBody
import okhttp3.RequestBody


interface ApiHelper {
    suspend fun getDashboard(token: String?): ShowroomDashboardBody
    suspend fun getBucketKomite(token: String?): BucketKomiteBody
    suspend fun getInfoShowroom(token: String?, idShowroom: String?): InfoShowroomBody

    suspend fun login(loginRequest: RequestBody): LoginResponseBody
    suspend fun loginLdap(ldapRequestBody: RequestBody): LdapBody


}