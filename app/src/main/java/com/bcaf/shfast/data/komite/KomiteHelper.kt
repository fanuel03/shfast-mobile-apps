package com.bcaf.shfast.data.komite

import com.bcaf.shfast.persistence.response.SHFDefaultResponse
import com.bcaf.shfast.persistence.response.dashboard.BucketKomiteBody
import com.bcaf.shfast.persistence.response.komite.DetailPengajuanKomite
import okhttp3.RequestBody

interface KomiteHelper {
    suspend fun getBucketKomite(token: String?): BucketKomiteBody
    suspend fun getDetailBucket(token: String?, body: RequestBody?): DetailPengajuanKomite

    suspend fun putApproval(token: String?, body: RequestBody?): SHFDefaultResponse
    suspend fun putReject(token: String?, body: RequestBody?): SHFDefaultResponse

    suspend fun history(token: String?, status: String?, sort: String?): BucketKomiteBody
    suspend fun getDetailHistory(token: String?, body: RequestBody?): DetailPengajuanKomite

    suspend fun logout(token: String?, body: RequestBody?): SHFDefaultResponse
}