package com.bcaf.shfast.data.showroom

import com.bcaf.shfast.persistence.response.SHFDefaultResponse
import com.bcaf.shfast.persistence.response.showroom.HistoryPengajuanBody
import com.bcaf.shfast.persistence.response.showroom.ListKontrakBody
import com.bcaf.shfast.persistence.response.showroom.ShowroomDashboardBody
import com.bcaf.shfast.persistence.response.showroom.UploadFileBody
import com.bcaf.shfast.persistence.response.showroom.history.HistorySHFBody
import com.bcaf.shfast.persistence.response.showroom.simulasi.SimulasiETBody
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.http.*

interface ShowroomService {
    @GET("/api/showroom/dashboard/?size=5")
    suspend fun getDashboard(
        @Header("Authorization") token: String?
    ): ShowroomDashboardBody

    @GET("/api/penggunaan/uuid/")
    suspend fun generateUuid(@Header("Authorization") token: String?): String

    @Multipart
    @POST("/api/uploadFile")
    suspend fun uploadFile(
        @Header("Authorization") token: String?,
        @Part img: MultipartBody.Part?
    ): UploadFileBody

    //Post Response
    @Headers("Content-Type:application/json")
    @POST("/api/penggunaan/")
    suspend fun pengajuan(
        @Header("Authorization") token: String?,
        @Body requestBody: RequestBody?
    ): SHFDefaultResponse

    @GET("api/penggunaan/listHistoryPenggunaan/{status}")
    suspend fun history(
        @Header("Authorization") token: String?,
        @Path("status") status: String?,
        @Query("sort") sort: String?
    ): HistoryPengajuanBody

    @POST("/api/showroom/detailHistoryPenggunaanSHF/")
    suspend fun detail(
        @Header("Authorization") token: String?,
        @Body requestBody: RequestBody?
    ): HistorySHFBody

    // Batal dong brooo
    @PUT("/api/penggunaan/batalPencairan/")
    suspend fun cancelRequest(
        @Header("Authorization") token: String?,
        @Body request: RequestBody?
    ): SHFDefaultResponse

    @POST("/api/penggunaan/getListKontrak/")
    suspend fun getListKontrak(
        @Header("Authorization") token: String?,
        @Body requestBody: RequestBody?
    ): ListKontrakBody

    @POST("/api/penggunaan/simulasiSHF")
    suspend fun getSimulasi(
        @Header("Authorization") token: String?,
        @Body request: RequestBody?
    ): SimulasiETBody

    //LogOut
    @POST("/api/showroom/logoutShowroom/")
    suspend fun logout(
        @Header("Authorization") token: String?,
        @Body request: RequestBody?
    ): SHFDefaultResponse
}