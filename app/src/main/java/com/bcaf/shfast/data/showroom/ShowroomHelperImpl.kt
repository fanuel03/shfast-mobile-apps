package com.bcaf.shfast.data.showroom

import com.bcaf.shfast.persistence.response.SHFDefaultResponse
import okhttp3.MultipartBody
import okhttp3.RequestBody

class ShowroomHelperImpl(private val apiService: ShowroomService): ShowroomHelper  {
    override suspend fun getDashboard(token: String?) = apiService.getDashboard(token)

    override suspend fun generateUuid(token: String?) = apiService.generateUuid(token)

    override suspend fun uploadFile(token: String?, img: MultipartBody.Part?) = apiService.uploadFile(token, img)

    override suspend fun pengajuan(token: String?, requestBody: RequestBody?) = apiService.pengajuan(token, requestBody)

    override suspend fun history(token: String?, status: String?, sort: String?) = apiService.history(token, status, sort)

    override suspend fun detail(token: String?, requestBody: RequestBody?) = apiService.detail(token, requestBody)

    override suspend fun cancel(token: String?, requestBody: RequestBody?) =
        apiService.cancelRequest(token, requestBody)

    override suspend fun getListKontrak(token: String?, body: RequestBody?) = apiService.getListKontrak(token, body)

    override suspend fun getSimulasi(token: String?, body: RequestBody?) = apiService.getSimulasi(token, body)

    override suspend fun logoout(token: String?, body: RequestBody?) = apiService.logout(token, body)

    override suspend fun changePassword(token: String?, body: RequestBody?): SHFDefaultResponse {
        TODO("Not yet implemented")
    }
}