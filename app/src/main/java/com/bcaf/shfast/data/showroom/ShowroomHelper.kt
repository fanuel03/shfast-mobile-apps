package com.bcaf.shfast.data.showroom

import com.bcaf.shfast.persistence.response.SHFDefaultResponse
import com.bcaf.shfast.persistence.response.showroom.HistoryPengajuanBody
import com.bcaf.shfast.persistence.response.showroom.ListKontrakBody
import com.bcaf.shfast.persistence.response.showroom.ShowroomDashboardBody
import com.bcaf.shfast.persistence.response.showroom.UploadFileBody
import com.bcaf.shfast.persistence.response.showroom.history.HistorySHFBody
import com.bcaf.shfast.persistence.response.showroom.simulasi.SimulasiETBody
import okhttp3.MultipartBody
import okhttp3.RequestBody

interface ShowroomHelper {
    suspend fun getDashboard(token: String?): ShowroomDashboardBody
    suspend fun generateUuid(token: String?): String
    suspend fun uploadFile(token: String?, img: MultipartBody.Part?): UploadFileBody
    suspend fun pengajuan(token: String?, requestBody: RequestBody?): SHFDefaultResponse
    suspend fun history(token: String?, status: String?, sort: String?): HistoryPengajuanBody

    suspend fun detail(token: String?, requestBody: RequestBody?): HistorySHFBody
    suspend fun cancel(token: String?, requestBody: RequestBody?): SHFDefaultResponse
    suspend fun getListKontrak(token: String?, body: RequestBody?): ListKontrakBody
    suspend fun getSimulasi(token: String?, body: RequestBody?): SimulasiETBody
    suspend fun logoout(token: String?, body: RequestBody?): SHFDefaultResponse

    suspend fun changePassword(token: String?, body: RequestBody?): SHFDefaultResponse
}