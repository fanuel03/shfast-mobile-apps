package com.bcaf.shfast.data

import com.bcaf.shfast.persistence.response.credential.LdapBody
import com.bcaf.shfast.persistence.response.credential.LoginResponseBody
import com.bcaf.shfast.persistence.response.dashboard.BucketKomiteBody
import com.bcaf.shfast.persistence.response.showroom.InfoShowroomBody
import com.bcaf.shfast.persistence.response.showroom.ShowroomDashboardBody
import okhttp3.RequestBody
import retrofit2.http.*

interface ApiService {

    @GET("/api/showroom/dashboard/")
    suspend fun getDashboard(
        @Header("Authorization") token: String?): ShowroomDashboardBody

    //Get List Bucket Komite
    @GET("/api/penggunaan/dashboardKomite/")
    suspend fun getBucketKomite(
        @Header("Authorization") token: String?
    ): BucketKomiteBody

    //Get Info Showroom
    @GET("/api/showroom/showroomInfo/{idShowroom}/")
    suspend fun getInfoShowroom(
        @Header("Authorization") token: String?,
        @Path("idShowroom") idShowroom: String?
    ): InfoShowroomBody

    //Verifikasi LDAP
    @POST("/api/penggunaan/login/LDAP/")
    suspend fun loginLdap(@Body request: RequestBody?): LdapBody


    @POST("/api/login")
    suspend fun login(@Body loginRequest: RequestBody?): LoginResponseBody
}