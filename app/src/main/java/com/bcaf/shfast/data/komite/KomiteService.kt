package com.bcaf.shfast.data.komite

import com.bcaf.shfast.persistence.response.SHFDefaultResponse
import com.bcaf.shfast.persistence.response.dashboard.BucketKomiteBody
import com.bcaf.shfast.persistence.response.komite.DetailPengajuanKomite
import okhttp3.RequestBody
import retrofit2.http.*

interface KomiteService {
    //Get List Bucket Komite
    @GET("/api/penggunaan/dashboardKomite/")
    suspend fun getBucketKomite(
        @Header("Authorization") token: String?): BucketKomiteBody

    //Detail Aprove
    @POST("/api/penggunaan/detailPenggunaanSHF/")
    suspend fun getDetailBucket(
        @Header("Authorization") token: String?,
        @Body requestBody: RequestBody?
    ): DetailPengajuanKomite

    //Detail Aprove
    @POST("/api/penggunaan/detailHistoryPencairan/")
    suspend fun getDetailHistory(
        @Header("Authorization") token: String?,
        @Body requestBody: RequestBody?
    ): DetailPengajuanKomite

    @POST("/api/penggunaan/rekomendasiOTR/")
    suspend fun putApproval(
        @Header("Authorization") token: String?,
        @Body requestBody: RequestBody?
    ): SHFDefaultResponse

    @PUT("/api/penggunaan/rejectPengajuan/")
    suspend fun putReject(
        @Header("Authorization") token: String?,
        @Body requestBody: RequestBody?
    ): SHFDefaultResponse

    //LogOut
    @POST("/api/showroom/logoutShowroom/")
    suspend fun logout(
        @Header("Authorization") token: String?,
        @Body request: RequestBody?
    ): SHFDefaultResponse

    @GET("/api/penggunaan/listHistoryKomite/{status}/")
    suspend fun history(
        @Header("Authorization") token: String?,
        @Path("status") status: String?,
        @Query("sort") sort: String?
    ): BucketKomiteBody

}