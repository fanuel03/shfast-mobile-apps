package com.bcaf.shfast.data

import okhttp3.RequestBody

class ApiHelperImpl(private val apiService: ApiService): ApiHelper {
    override suspend fun getDashboard(token: String?) = apiService.getDashboard(token)

    override suspend fun getBucketKomite(token: String?) = apiService.getBucketKomite(token)

    override suspend fun getInfoShowroom(token: String?, idShowroom: String?) = apiService.getInfoShowroom(token, idShowroom)

    override suspend fun login(loginRequest: RequestBody) = apiService.login(loginRequest)
    override suspend fun loginLdap(ldapRequestBody: RequestBody) = apiService.loginLdap(ldapRequestBody)
}