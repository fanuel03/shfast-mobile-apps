package com.bcaf.shfast

import android.app.Application
import com.bcaf.shfast.di.module.appModule
import com.bcaf.shfast.di.module.repoModule
import com.bcaf.shfast.di.module.viewModelModule
import com.bcaf.shfast.di.module.listenerModule

import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class App: Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@App)
            modules(listOf(appModule, repoModule, viewModelModule, listenerModule))
        }
    }
}