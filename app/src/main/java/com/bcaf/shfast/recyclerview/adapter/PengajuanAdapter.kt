package com.bcaf.shfast.recyclerview.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bcaf.shfast.R
import com.bcaf.shfast.persistence.model.Pengajuan
import com.bcaf.shfast.recyclerview.callback.DetailCallback
import com.bcaf.shfast.recyclerview.viewholder.PengajuanViewHolder
import java.util.*

class PengajuanAdapter(
    private val list: ArrayList<Pengajuan> = ArrayList(),
    private val context: Context,
    private val callback: DetailCallback
): RecyclerView.Adapter<PengajuanViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PengajuanViewHolder {
        val view: View = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_pengajuan, parent, false)

        return PengajuanViewHolder(view)
    }

    override fun getItemCount() = list.size

    override fun onBindViewHolder(holder: PengajuanViewHolder, position: Int) {
        holder.callback = callback
        holder.setPengajuan(list[position])
    }
}