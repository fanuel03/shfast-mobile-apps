package com.bcaf.shfast.recyclerview.viewholder

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.bcaf.shfast.persistence.model.Kontrak
import com.bcaf.shfast.recyclerview.callback.ContractCallback
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_kontrak.*

class ContractViewHolder (
    itemView: View
) : RecyclerView.ViewHolder(itemView), LayoutContainer {

    var callback: ContractCallback? = null

    override val containerView: View?
        get() = itemView

    fun setContract(kontrak: Kontrak) {
        noKontrak.text = kontrak.noKontrak ?: "-"
        merk.text = kontrak.merkMobil ?: "-"
        tipe.text = kontrak.tipeMobil ?: "-"
        noPlat.text = kontrak.noPolisi ?: "-"
        container_kontrak.setOnClickListener {
            callback!!.onClick(kontrak)
        }
    }

}