package com.bcaf.shfast.recyclerview.viewholder

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.bcaf.shfast.persistence.model.komite.Penyimpangan
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_penyimpangan.*

class PenyimpanganViewHolder (
    itemView: View
) : RecyclerView.ViewHolder(itemView), LayoutContainer {

    override val containerView: View?
        get() = itemView

    fun setPenyimpangan(penyimpangan: Penyimpangan) {
        headerPenyimpangan.text = penyimpangan.toHeader()
        deskripsiPenyimpangan.text = penyimpangan.deskripsi
    }

}

