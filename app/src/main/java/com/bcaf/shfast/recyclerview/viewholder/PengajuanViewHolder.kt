package com.bcaf.shfast.recyclerview.viewholder

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.bcaf.shfast.R
import com.bcaf.shfast.ext.toRupiah
import com.bcaf.shfast.persistence.model.Pengajuan
import com.bcaf.shfast.recyclerview.callback.DetailCallback
import com.bcaf.shfast.util.SHFConstant
import com.bcaf.shfast.util.SHFUtil
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_pengajuan.*

class PengajuanViewHolder(
    itemView: View
) : RecyclerView.ViewHolder(itemView), LayoutContainer {
    var callback: DetailCallback? = null

    override val containerView: View?
        get() = itemView

    fun setPengajuan(pengajuan: Pengajuan) {
        lable.visibility = View.VISIBLE
        tipeMobil.text = pengajuan.tipe
        noPlat.text = pengajuan.noPlat
        nominal.text = pengajuan.nominal?.toRupiah()

        when {
            pengajuan.status.equals("Rejected", ignoreCase = true) -> {
                step.text = SHFConstant.STEP_REJECTED
                status.setImageResource(R.drawable.ic_block)
            }
            pengajuan.status.equals("Final-Approved", ignoreCase = true) -> {
                step.text = SHFConstant.STEP_APPROVED
                status.setImageResource(R.drawable.ic_check)
            }
            else -> {
                step.text = SHFConstant.STEP_REQUEST

                when {
                    pengajuan.status.equals("Pending", ignoreCase = true) -> {
                        status.setImageResource(R.drawable.ic_proses)
                    }
                    pengajuan.status.equals("Canceled", ignoreCase = true) -> {
                        status.setImageResource(R.drawable.ic_cancel)
                    }
                    else -> {
                        status.setImageResource(R.drawable.ic_proses)
                    }
                }

            }
        }

        status2.text = pengajuan.status.split("-").firstOrNull() ?: ""
        tanggal.text = SHFUtil.parseTime(pengajuan.tanggal)

        detail.setOnClickListener {
            callback?.onClick(pengajuan.idPengajuan)
        }
    }


}