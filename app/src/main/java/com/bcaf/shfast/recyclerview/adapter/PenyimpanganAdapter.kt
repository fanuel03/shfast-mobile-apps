package com.bcaf.shfast.recyclerview.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bcaf.shfast.R
import com.bcaf.shfast.persistence.model.komite.Penyimpangan
import com.bcaf.shfast.recyclerview.viewholder.PenyimpanganViewHolder
import java.util.*

class PenyimpanganAdapter(
    private val list: ArrayList<Penyimpangan> = ArrayList(),
    private val context: Context
): RecyclerView.Adapter<PenyimpanganViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PenyimpanganViewHolder {
        val view: View = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_penyimpangan, parent, false)

        return PenyimpanganViewHolder(view)
    }

    override fun getItemCount() = list.size

    override fun onBindViewHolder(holder: PenyimpanganViewHolder, position: Int) {
        holder.setPenyimpangan(list[position])
    }
}