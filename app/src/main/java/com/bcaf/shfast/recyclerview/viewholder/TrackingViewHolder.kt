package com.bcaf.shfast.recyclerview.viewholder

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.bcaf.shfast.ext.toRupiah
import com.bcaf.shfast.persistence.response.komite.StatusRekomendasi
import com.bcaf.shfast.util.SHFUtil
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_tracking.*
import java.util.*

class TrackingViewHolder(
    itemView: View
) : RecyclerView.ViewHolder(itemView), LayoutContainer  {
    override val containerView: View?
        get() = itemView

    fun setTracking(tracking: StatusRekomendasi) {
        trackingRole.text = tracking.recomendBy ?: "-"
        trackingStatus.text = tracking.status ?: "-"
        tracking.tanggal?.let {
            trackingDate.text = SHFUtil.parseTrackingTime(it)
        }

        when(tracking.status) {
            "Awc" -> {
                trackingStatus.text = trackingStatus.text.toString().toUpperCase(Locale.ROOT)
                trackingDescription.text = tracking.keterangan ?: "-"
                tracking.otrRekomen?.let {
                    trackingPrice.text =  it.toString().toRupiah()
                }
            }
            "Rejected" -> {
                trackingDescription.text = tracking.keterangan ?: "-"
                containerTrackingPrice.visibility = View.GONE
            }
            else -> {
                containerTrackingPrice.visibility = View.GONE
                containerTrackingDescription.visibility = View.GONE
            }
        }

        if(!tracking.lastIndex) {
            viewGuide.visibility = View.VISIBLE
        }

    }
}