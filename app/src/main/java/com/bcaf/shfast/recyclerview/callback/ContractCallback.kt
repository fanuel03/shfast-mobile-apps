package com.bcaf.shfast.recyclerview.callback

import com.bcaf.shfast.persistence.model.Kontrak

interface ContractCallback {
    fun onClick(kontrak: Kontrak)
}