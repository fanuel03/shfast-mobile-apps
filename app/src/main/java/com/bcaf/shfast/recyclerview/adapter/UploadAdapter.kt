package com.bcaf.shfast.recyclerview.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bcaf.shfast.R
import com.bcaf.shfast.persistence.model.Dokumen
import com.bcaf.shfast.recyclerview.callback.UploadCallback
import com.bcaf.shfast.recyclerview.viewholder.UploadViewHolder
import java.util.*

class UploadAdapter(
    private val list: ArrayList<Dokumen> = ArrayList(),
    private val context: Context,
    private val callback: UploadCallback
): RecyclerView.Adapter<UploadViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UploadViewHolder {
        val view: View = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_upload, parent, false)

        return UploadViewHolder(view)
    }

    override fun getItemCount() = list.size

    override fun onBindViewHolder(holder: UploadViewHolder, position: Int) {
        holder.callback = callback
        holder.setUploadDokumen(list[position], position, context)
    }
}