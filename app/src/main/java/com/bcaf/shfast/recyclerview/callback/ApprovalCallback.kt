package com.bcaf.shfast.recyclerview.callback

import com.bcaf.shfast.persistence.model.komite.PengajuanKomite

interface ApprovalCallback {
    fun onClick(pengajuan: PengajuanKomite)
}