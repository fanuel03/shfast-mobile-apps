package com.bcaf.shfast.recyclerview.callback

interface DetailCallback {
    fun onClick(id: String)
}