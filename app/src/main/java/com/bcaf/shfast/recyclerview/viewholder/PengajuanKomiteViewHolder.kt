package com.bcaf.shfast.recyclerview.viewholder

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.bcaf.shfast.persistence.model.komite.PengajuanKomite
import com.bcaf.shfast.recyclerview.callback.ApprovalCallback
import com.bcaf.shfast.util.SHFUtil
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_pengajuan_komite.*

class PengajuanKomiteViewHolder (
    itemView: View
) : RecyclerView.ViewHolder(itemView), LayoutContainer {

    var callback: ApprovalCallback? = null

    override val containerView: View?
        get() = itemView

    fun setPengajuanKomite(pengajuan: PengajuanKomite) {
        namaShowroom.text = pengajuan.namaShowroom
        tipeMobil.text = "${pengajuan.tipeMobil} ${pengajuan.tahunMobil}"
        platNomor.text = pengajuan.noPlat
        tgl_pengajuan.text = SHFUtil.parseTime(pengajuan.tglPengajuan)

        container_pengajuan_komite.setOnClickListener {
            callback?.onClick(pengajuan)
        }
    }

}