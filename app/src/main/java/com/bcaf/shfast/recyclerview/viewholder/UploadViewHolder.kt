package com.bcaf.shfast.recyclerview.viewholder

import android.content.Context
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.bcaf.shfast.R
import com.bcaf.shfast.persistence.model.Dokumen
import com.bcaf.shfast.recyclerview.callback.UploadCallback
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_upload.*

class UploadViewHolder(
    itemView: View
) : RecyclerView.ViewHolder(itemView), LayoutContainer {
    var callback: UploadCallback? = null

    override val containerView: View?
        get() = itemView

    fun setUploadDokumen(dokumen: Dokumen, position: Int, ctx: Context) {
        lblBpkb.text = dokumen.tag!!
        if (dokumen.path != null && dokumen.path?.isNotBlank()!!) {
            card_upload.setCardBackgroundColor(ctx.resources.getColor( R.color.transparent_green))
            btnBpkb.setBackgroundColor(ctx.resources.getColor( R.color.transparent_green))
            imgCheck.visibility = View.VISIBLE
        } else {
            card_upload.setCardBackgroundColor(ctx.resources.getColor( R.color.white))
            btnBpkb.setBackgroundColor(ctx.resources.getColor( R.color.white))
            imgCheck.visibility = View.INVISIBLE
        }

        btnBpkb.setOnClickListener {
            callback?.onAddImage(position)
        }
    }
}