package com.bcaf.shfast.recyclerview.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bcaf.shfast.R
import com.bcaf.shfast.persistence.model.Kontrak
import com.bcaf.shfast.recyclerview.callback.ContractCallback
import com.bcaf.shfast.recyclerview.viewholder.ContractViewHolder

class ContractAdapter (
    private val list: ArrayList<Kontrak> = ArrayList(),
    private val context: Context,
    private val callback: ContractCallback
): RecyclerView.Adapter<ContractViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContractViewHolder {
        val view: View = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_kontrak, parent, false)

        return ContractViewHolder(view)

    }

    override fun getItemCount() = list.size

    override fun onBindViewHolder(holder: ContractViewHolder, position: Int) {
        holder.callback = callback
        holder.setContract(list[position])
    }
}