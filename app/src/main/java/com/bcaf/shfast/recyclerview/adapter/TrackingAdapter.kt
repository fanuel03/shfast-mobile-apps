package com.bcaf.shfast.recyclerview.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bcaf.shfast.R
import com.bcaf.shfast.persistence.response.komite.StatusRekomendasi
import com.bcaf.shfast.recyclerview.viewholder.TrackingViewHolder
import java.util.*

class TrackingAdapter(
    private val list: ArrayList<StatusRekomendasi> = ArrayList(),
    private val context: Context): RecyclerView.Adapter<TrackingViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TrackingViewHolder {
        val view: View = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_tracking, parent, false)

        return TrackingViewHolder(view)
    }

    override fun getItemCount() = list.size

    override fun onBindViewHolder(holder: TrackingViewHolder, position: Int) {
        holder.setTracking(list[position])
    }
}