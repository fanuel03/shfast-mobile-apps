package com.bcaf.shfast.recyclerview.callback

import com.bcaf.shfast.persistence.model.Dokumen

interface UploadCallback {
    fun onAddImage(position: Int)
}