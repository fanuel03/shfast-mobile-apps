package com.bcaf.shfast.recyclerview.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bcaf.shfast.R
import com.bcaf.shfast.persistence.model.komite.PengajuanKomite
import com.bcaf.shfast.recyclerview.callback.ApprovalCallback
import com.bcaf.shfast.recyclerview.viewholder.PengajuanKomiteViewHolder
import java.util.*

class PengajuanKomiteAdapter (private val list: ArrayList<PengajuanKomite> = ArrayList(),
                              private val context: Context,
                              private val callback: ApprovalCallback
): RecyclerView.Adapter<PengajuanKomiteViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PengajuanKomiteViewHolder {
        val view: View = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_pengajuan_komite, parent, false)

        return PengajuanKomiteViewHolder(view)
    }

    override fun getItemCount() = list.size

    override fun onBindViewHolder(holder: PengajuanKomiteViewHolder, position: Int) {
        holder.callback = callback
        holder.setPengajuanKomite(list[position])
    }
}