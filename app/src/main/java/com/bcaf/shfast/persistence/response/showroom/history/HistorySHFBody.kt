package com.bcaf.shfast.persistence.response.showroom.history

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class HistorySHFBody (
    @SerializedName("detail-history")
    @Expose
    val historyStatus: DetailHistory? = null
)