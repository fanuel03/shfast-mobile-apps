package com.bcaf.shfast.persistence.response.showroom

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class RekeningShowroom (
    @SerializedName("AccNo")
    @Expose
    var accNo: String? = null,

    @SerializedName("AccAtasNama")
    @Expose
    var accAtasNama: String? = null,

    @SerializedName("AccBankName")
    @Expose
    var accBankName: String? = null,

    @SerializedName("AccBranch")
    @Expose
    var accBranch: String? = null,

    @SerializedName("DefaultSts")
    @Expose
    var defaultSts: String? = null
    )