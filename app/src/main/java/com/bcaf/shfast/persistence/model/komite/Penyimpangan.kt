package com.bcaf.shfast.persistence.model.komite

data class Penyimpangan(
    val level: String,
    val kode: String,
    val deskripsi: String
) {
    fun toHeader(): String {
        return "LEVEL $level: $kode"
    }
}