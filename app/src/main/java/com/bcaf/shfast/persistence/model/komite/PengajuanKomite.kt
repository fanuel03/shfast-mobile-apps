package com.bcaf.shfast.persistence.model.komite

import com.bcaf.shfast.persistence.response.komite.StatusRekomendasi
import java.io.Serializable

data class PengajuanKomite (
    var idPenggunaan: String = "",
    var namaShowroom: String = "",
    var noPlat: String = "",
    var noTlp: String = "",
    var email: String = "",
    var wilayah: String = "",
    var tglPengajuan: String = "",
    var tahunRekanan: String = "0",
    var merkMobil: String = "",
    var tipeMobil: String = "",
    var warnaMobil: String = "",
    var tahunMobil: String = "",
    var tahunBerdiri: String = "0",
    var tracking: List<StatusRekomendasi>? = null
): Serializable {

}