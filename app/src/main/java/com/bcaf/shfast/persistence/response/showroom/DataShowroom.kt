package com.bcaf.shfast.persistence.response.showroom

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class DataShowroom (
    @SerializedName("sisaPlafond")
    @Expose
    var sisaPlafond: Double? = null,

    @SerializedName("lamaUsaha")
    @Expose
    var lamaUsaha: String? = null,

    @SerializedName("idShowroom")
    @Expose
    var idShowroom: Int? = null,

    @SerializedName("noTlp")
    @Expose
    var noTlp: String? = null,

    @SerializedName("lamaRekanan")
    @Expose
    var lamaRekanan: String? = null,

    @SerializedName("namaShowroom")
    @Expose
    var namaShowroom: String? = null,

    @SerializedName("wilayah")
    @Expose
    var wilayah: String? = null,

    @SerializedName("status")
    @Expose
    var status: String? = null

)
