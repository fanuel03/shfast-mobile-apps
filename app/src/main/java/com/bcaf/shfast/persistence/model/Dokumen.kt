package com.bcaf.shfast.persistence.model

data class Dokumen(
    var tag: String? = null,
    var name: String? = null,
    var path: String? = null,
    var paramName: String? = null,
    var downloadUri: String? = null
)