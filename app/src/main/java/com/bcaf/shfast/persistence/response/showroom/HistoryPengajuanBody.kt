package com.bcaf.shfast.persistence.response.showroom

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class HistoryPengajuanBody (
    @SerializedName("success")
    @Expose
    var success: String? = null,

    @SerializedName("history-penggunaan-shf")
    @Expose
    var listHistoryPenggunaan: List<ListHistoryPengajuanSHF>? = null

)