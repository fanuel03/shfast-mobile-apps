package com.bcaf.shfast.persistence.response.credential

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class LoginResponseBody (
    @SerializedName("firstName")
    @Expose
    var firstName: String? = null,

    @SerializedName("lastName")
    @Expose
    var wilayah: String? = null,

    @SerializedName("role")
    @Expose
    var role: String? = null,

    @SerializedName("succeed")
    @Expose
    var succeed: String? = null,

    @SerializedName("id")
    @Expose
    var id: Int? = null,

    @SerializedName("username") // email - Showroom // komite - NIP
    @Expose
    var username: String? = null,

    @SerializedName("token")
    @Expose
    var token: String? = null

)
