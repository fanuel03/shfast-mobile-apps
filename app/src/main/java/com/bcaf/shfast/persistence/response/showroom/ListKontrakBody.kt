package com.bcaf.shfast.persistence.response.showroom

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class ListKontrakBody (
    @SerializedName("ResponseHeader")
    @Expose
    var responseHeader: FinaResponseHeader? = null,

    @SerializedName("NoKontrakList")
    @Expose
    val kontrakList: KontrakList? = null
)