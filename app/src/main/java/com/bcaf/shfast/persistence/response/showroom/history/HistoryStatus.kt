package com.bcaf.shfast.persistence.response.showroom.history

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class HistoryStatus (
    @SerializedName("kondisi")
    @Expose
    private var kondisi: String? = null,

    @SerializedName("tanggal")
    @Expose
    private val tanggal: String? = null,

    @SerializedName("status")
    @Expose
    private val status: String? = null

)