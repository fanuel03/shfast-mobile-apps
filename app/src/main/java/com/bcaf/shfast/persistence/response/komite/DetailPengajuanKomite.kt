package com.bcaf.shfast.persistence.response.komite

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class DetailPengajuanKomite (

    @SerializedName("idShowroom")
    @Expose
    val idShowroom: String? = null,

    @SerializedName("fotoTampakBelakang")
    @Expose
    val fotoTampakBelakang: String? = null,

    @SerializedName("tahunMobil")
    @Expose
    val tahunMobil: String? = null,

    @SerializedName("list_status_rekomen")
    @Expose
    val statusRekomendasi: List<StatusRekomendasi>? = null,

    @SerializedName("merk")
    @Expose
    val merk: String? = null,

    @SerializedName("fotoSTNK")
    @Expose
    val fotoSTNK: String? = null,

    @SerializedName("fotoFaktur")
    @Expose
    val fotoFaktur: String? = null,

    @SerializedName("fotoTampakKanan")
    @Expose
    val fotoTampakKanan: String? = null,

    @SerializedName("pengalamanUsaha")
    @Expose
    val pengalamanUsaha: String? = null,

    @SerializedName("hubunganBCAF")
    @Expose
    val hubunganBCAF: String? = null,

    @SerializedName("wilayahShowroom")
    @Expose
    val wilayahShowroom: String? = null,

    @SerializedName("type")
    @Expose
    val type: String? = null,

    @SerializedName("noPol")
    @Expose
    val noPol: String? = null,

    @SerializedName("fotoBPKB")
    @Expose
    val fotoBPKB: String? = null,

    @SerializedName("createdAt")
    @Expose
    val createdAt: String? = null,

    @SerializedName("warnaMobil")
    @Expose
    val warnaMobil: String? = null,

    @SerializedName("jumlahPenggunaan")
    @Expose
    val jumlahPenggunaan: Double? = null,

    @SerializedName("noTlp")
    @Expose
    val noTlp: String? = null,

    @SerializedName("fotoTampakKiri")
    @Expose
    val fotoTampakKiri: String? = null,

    @SerializedName("namaShowroom")
    @Expose
    val namaShowroom: String? = null,

    @SerializedName("fotoTampakDepan")
    @Expose
    val fotoTampakDepan: String? = null,

    @SerializedName("tahunKendaraan")
    @Expose
    val year: String? = null
)