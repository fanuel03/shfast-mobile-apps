package com.bcaf.shfast.persistence.response.dashboard

import com.bcaf.shfast.persistence.model.komite.PengajuanKomite
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class ListPenggunaan (
    @SerializedName("CreatedAt")
    @Expose
    var createdAt: String? = null,

    @SerializedName("IdPenggunaan")
    @Expose
    var idPenggunaan: String? = null,

    @SerializedName("Merk")
    @Expose
    var merk: String? = null,

    @SerializedName("NamaShowroom")
    @Expose
    var namaShowroom: String? = null,

    @SerializedName("TahunMobil")
    @Expose
    var tahunMobil: String? = null,

    @SerializedName("Type")
    @Expose
    var type: String? = null,

    @SerializedName("NoPol")
    @Expose
    var noPlat: String? = null

) {
    fun toPengajuanKomite(): PengajuanKomite {
        return PengajuanKomite(
            idPenggunaan = idPenggunaan!!,
            noPlat = noPlat!!,
            tahunMobil = tahunMobil ?: "",
            namaShowroom = namaShowroom ?: "-",
            tipeMobil = type ?: "-",
            tglPengajuan = createdAt ?: "-"
        )
    }
}