package com.bcaf.shfast.persistence.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class SHFDefaultResponse (
    @SerializedName("success")
    @Expose
    var success: String? = null,

    @SerializedName("message")
    @Expose
    var message: String? = null,

    @SerializedName("status")
    @Expose
    var status: String? = null
)
