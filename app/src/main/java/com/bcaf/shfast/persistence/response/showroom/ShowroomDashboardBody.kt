package com.bcaf.shfast.persistence.response.showroom

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class ShowroomDashboardBody (
    @SerializedName("idShowroom")
    @Expose
    var idShowroom: Int? = null,

    @SerializedName("plafond")
    @Expose
    var plafond: Double? = null,

    @SerializedName("exposure")
    @Expose
    var exposure: Double? = null,

    @SerializedName("kontribusi")
    @Expose
    var kontribusi: Double? = null,

    @SerializedName("npl")
    @Expose
    var npl: Double? = null,

    @SerializedName("NIK")
    @Expose
    var nik: String? = null,

    @SerializedName("masaBerlaku")
    var masaBerlaku: String? = null,

    @SerializedName("history-penggunaan-shf")
    @Expose
    var historyPenggunaanShf: List<ListHistoryPengajuanSHF>? = null,

    @SerializedName("jumlahActiveAccount")
    @Expose
    var jumlahAkunAktif: Int? = null,

    @SerializedName("jumlahAccount")
    @Expose
    var jumlahAccount: Int? = null,

    @SerializedName("noRek")
    @Expose
    var noRekening: List<RekeningShowroom>? = null


    )
