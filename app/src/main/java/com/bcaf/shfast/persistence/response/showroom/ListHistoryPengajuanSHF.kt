package com.bcaf.shfast.persistence.response.showroom

import com.bcaf.shfast.persistence.model.Pengajuan
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class ListHistoryPengajuanSHF (
    @SerializedName("createdAt")
    @Expose
    private val createdAt: String? = null,

    @SerializedName("idShowroom")
    @Expose
    private val idShowroom: Int? = null,

    @SerializedName("jumlahPenggunaan")
    @Expose
    private val jumlahPenggunaan: Long? = null,

    @SerializedName("noPlat")
    @Expose
    private val noPlat: String? = null,

    @SerializedName("status")
    @Expose
    private val status: String? = null,

    @SerializedName("tahunMobil")
    @Expose
    private val tahunMobil: String? = null,

    @SerializedName("type")
    @Expose
    private val type: String? = null,

    @SerializedName("idPenggunaan")
    @Expose
    private val idPenggunaan: String? = null
) {
    fun toPengajuan(): Pengajuan {
        return Pengajuan(
            idPengajuan = idPenggunaan ?: "",
            status = status?: "",
            tanggal = createdAt ?: "",
            alasan = "",
            nominal = jumlahPenggunaan ?: 0,
            noPlat = noPlat ?: "",
            tipe = type ?: " $tahunMobil" ?: ""
        )
    }
}