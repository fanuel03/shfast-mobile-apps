package com.bcaf.shfast.persistence.response.showroom

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class UploadFileBody (
    @SerializedName("fileName")
    @Expose
    var fileName: String? = null,

    @SerializedName("fileDownloadUri")
    @Expose
    var fileDownloadUri: String? = null,

    @SerializedName("fileType")
    @Expose
    var fileType: String? = null,

    @SerializedName("size")
    @Expose
    var size: Int? = null,

    @SerializedName("status")
    @Expose
    var status: String? = null
)
