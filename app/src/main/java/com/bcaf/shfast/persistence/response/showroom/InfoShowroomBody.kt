package com.bcaf.shfast.persistence.response.showroom

import com.bcaf.shfast.persistence.response.showroom.DataShowroom
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class InfoShowroomBody (
    @SerializedName("data-showroom")
    @Expose
    var dataShowroom: DataShowroom? = null,

    @SerializedName("status")
    @Expose
    var status: String? = null

)
