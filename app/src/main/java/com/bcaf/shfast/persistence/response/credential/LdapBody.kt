package com.bcaf.shfast.persistence.response.credential

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class LdapBody {
//    @SerializedName("ResponseHeader")
//    @Expose
//    var responseHeader: LdapResponseHeader? = null

    @SerializedName("Login")
    @Expose
    var loginResponseBody: LoginResponseBody? = null

}
