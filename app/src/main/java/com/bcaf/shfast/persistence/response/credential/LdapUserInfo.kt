package com.bcaf.shfast.persistence.response.credential

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class LdapUserInfo {
    @SerializedName("UserId")
    @Expose
    var userId: String? = null

    @SerializedName("FullName")
    @Expose
    var fullname: String? = null

}
