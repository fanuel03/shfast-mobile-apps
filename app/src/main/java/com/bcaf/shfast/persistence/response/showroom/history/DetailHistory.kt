package com.bcaf.shfast.persistence.response.showroom.history

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class DetailHistory (
    @SerializedName("history-status") @Expose
    var historyStatus: List<HistoryStatus?>? = null,

    @SerializedName("data")
    @Expose
    val data: DataHistory? = null
)