package com.bcaf.shfast.persistence.response.dashboard

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class BucketKomiteBody (
    @SerializedName("listPenggunaan")
    @Expose
    var listPenggunaan: List<ListPenggunaan>? = null

)