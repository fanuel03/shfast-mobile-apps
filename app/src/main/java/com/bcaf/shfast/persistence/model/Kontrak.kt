package com.bcaf.shfast.persistence.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Kontrak (
    @SerializedName("NoKontrak")
    @Expose
    var noKontrak: String? = null,

    @SerializedName("NIK")
    @Expose
    val nik: String? = null,

    @SerializedName("MerkKendaraan")
    @Expose
    val merkMobil: String? = null,

    @SerializedName("TipeKendaraan")
    @Expose
    val tipeMobil: String? = null,

    @SerializedName("NoPolisi")
    @Expose
    val noPolisi: String? = null,

    @SerializedName("TanggalReleased")
    @Expose
    val tglReleased: String? = null,

    @SerializedName("TanggalClosed")
    @Expose
    val tglClosed: String? = null,

    @SerializedName("DealerID")
    @Expose
    private val dealerID: String? = null
)