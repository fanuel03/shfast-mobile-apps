package com.bcaf.shfast.persistence.response.komite

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class StatusRekomendasi (
    @SerializedName("recomend_by")
    @Expose
    val recomendBy: String? = null,

    @SerializedName("keterangan")
    @Expose
    val keterangan: String? = null,

    @SerializedName("tanggal")
    @Expose
    val tanggal: String? = null,

    @SerializedName("otr_rekomen")
    @Expose
    val otrRekomen: Double? = null,

    @SerializedName("userID")
    @Expose
    val userID: String? = null,

    @SerializedName("status")
    @Expose
    val status: String? = null,

    var lastIndex: Boolean = false
)