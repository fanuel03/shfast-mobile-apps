package com.bcaf.shfast.persistence.response.showroom.simulasi

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class SimulasiETBody (
    /*

    Response :
{
    "ETSHFastRequest" : {
        "VirtualAccountNo" : "",
        "Bunga" : "0.00000123",
        "PokokHutang" : "1234545456.3",
        "BiayaAdmin" : "50000.000",
        "Denda" : "0",
        "TotalSimulasiET" : "213142344"
    }

}
     */
    @SerializedName("ETSHFastResponse")
    @Expose
    var etshFastResponse: ETSHFastResponse? = null

)