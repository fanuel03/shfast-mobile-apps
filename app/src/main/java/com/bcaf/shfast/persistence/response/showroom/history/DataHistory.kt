package com.bcaf.shfast.persistence.response.showroom.history

import com.bcaf.shfast.persistence.response.komite.StatusRekomendasi
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class DataHistory (
    @SerializedName("warna")
    @Expose
    var warna: String? = null,

    @SerializedName("merk")
    @Expose
    val merk: String? = null,

    @SerializedName("idShowroom")
    @Expose
    val idShowroom: Int? = null,

    @SerializedName("tahun")
    @Expose
    val tahun: String? = null,

    @SerializedName("otr-tmp")
    @Expose
    val otrTmp: Double? = null,

    @SerializedName("noPlat")
    @Expose
    val noPlat: String? = null,

    @SerializedName("otr-rekomen")
    @Expose
    val otrRekomen: Double? = null,

    @SerializedName("tanggalPenggunaan")
    @Expose
    val tanggalPenggunaan: String? = null,

    @SerializedName("tipe")
    @Expose
    val tipe: String? = null,

    @SerializedName("status")
    @Expose
    val status: String? = null,

    @SerializedName("keterangan-pic-shf")
    @Expose
    val keteranganPic: String? = null,


    @SerializedName("list_status_rekomen")
    @Expose
    val statusRekomendasi: List<StatusRekomendasi>? = null


    )