package com.bcaf.shfast.persistence.model

data class Pengajuan (
    var idPengajuan: String = "",
    var status: String = "",
    var tanggal: String = "",
    var alasan: String = "",
    var nominal: Long? = null,
    var tipe: String = "",
    var noPlat: String = ""
)
