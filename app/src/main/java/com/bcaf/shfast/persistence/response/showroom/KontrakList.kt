package com.bcaf.shfast.persistence.response.showroom

import com.bcaf.shfast.persistence.model.Kontrak
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class KontrakList (
    @SerializedName("NoKontrak") @Expose
    var kontrak: List<Kontrak>? = null
)