package com.bcaf.shfast.persistence.response.showroom

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class FinaResponseHeader (
    @SerializedName("ErrorCode")
    @Expose
    var errorCode: String? = null,

    @SerializedName("ErrorDescription")
    @Expose
    val errorDesc: String? = null,

    @SerializedName("TrxId")
    @Expose
    private val trxID: String? = null
)