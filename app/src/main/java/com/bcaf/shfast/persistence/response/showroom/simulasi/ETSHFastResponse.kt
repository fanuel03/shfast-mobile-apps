package com.bcaf.shfast.persistence.response.showroom.simulasi

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class ETSHFastResponse (
    /*
"ETSHFastRequest" : {
        "VirtualAccountNo" : "",
        "Bunga" : "0.00000123",
        "PokokHutang" : "1234545456.3",
        "BiayaAdmin" : "50000.000",
        "Denda" : "0",
        "TotalSimulasiET" : "213142344"
    }
 */
    @SerializedName("VirtualAccountNo")
    @Expose
    var virtualAccountNo: String? = null,

    @SerializedName("Bunga")
    @Expose
    var bunga: String? = null,

    @SerializedName("PokokHutang")
    @Expose
    var pokokHutang: String? = null,

    @SerializedName("BiayaAdmin")
    @Expose
    var biayaAdmin: String? = null,

    @SerializedName("Denda")
    @Expose
    var denda: String? = null,

    @SerializedName("TotalSimulasiET")
    @Expose
    var totalSimulasiET: String? = null
) {
    fun cleanseData() {
        virtualAccountNo?.let {
            virtualAccountNo = it.split(" ")[0]
        }

        bunga?.let {
            bunga = it.split(" ")[0]
        }

        pokokHutang?.let {
            pokokHutang = it.split(" ")[0]
        }

        biayaAdmin?.let {
            biayaAdmin = it.split(" ")[0]
        }

        denda?.let {
            denda = it.split(" ")[0]
        }

        totalSimulasiET?.let {
            totalSimulasiET  = it.split(" ")[0]
        }
    }
}