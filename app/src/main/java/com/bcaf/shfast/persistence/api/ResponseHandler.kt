package com.bcaf.shfast.persistence.api

import android.content.Context
import android.content.Intent
import com.bcaf.shfast.ui.LoginActivity
import com.bcaf.shfast.util.SHFConstant
import com.bcaf.shfast.util.SessionManager
import com.google.firebase.crashlytics.FirebaseCrashlytics
import retrofit2.HttpException
import java.net.SocketTimeoutException

open class ResponseHandler(
    private val context: Context
) {
    fun <T : Any> handleSuccess(data: T): Resource<T> {
        return Resource.success(data)
    }

    fun <T : Any> handleException(e: Exception): Resource<T> {
        FirebaseCrashlytics.getInstance().recordException(e)
        return when (e) {
            is HttpException -> Resource.error(getErrorMessage(e.code()), null)
            is SocketTimeoutException -> Resource.error(getErrorMessage(408), null)
            else -> Resource.error(getErrorMessage(Int.MAX_VALUE), null)
        }
    }

    private fun getErrorMessage(code: Int): String {
        if (code == 403) {
            // Kick
            SessionManager.flush(context)
            context.startActivity(LoginActivity.getIntent(context).apply {
                flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            })

        }
        return when (code) {
            400 -> "Username atau Password Salah"
            408 -> "Request Timeout"
            401 -> "Unauthorized Request"
            404 -> "URL not found"
            403 -> SHFConstant.HTTP_CODE_403
            else -> "Something went wrong"
        }
    }
}
