package com.bcaf.shfast.persistence.response.credential

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class LdapResponseHeader {
    @SerializedName("ErrorCode")
    @Expose
    var errorCode: String? = null

    @SerializedName("ErrorDescription")
    @Expose
    var errorDesc: String? = null

    @SerializedName("TrxId")
    @Expose
    var trxid: String? = null

}
