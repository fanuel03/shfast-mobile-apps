package com.bcaf.shfast.viewmodel

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.bcaf.shfast.persistence.api.Resource
import com.bcaf.shfast.persistence.api.Status
import com.bcaf.shfast.persistence.response.credential.LdapBody
import com.bcaf.shfast.persistence.response.credential.LoginResponseBody
import com.bcaf.shfast.repository.LoginRepository
import com.bcaf.shfast.util.RequestUtil
import com.bcaf.shfast.util.SHFConstant
import com.bcaf.shfast.util.SessionManager
import com.google.firebase.crashlytics.FirebaseCrashlytics
import kotlinx.coroutines.launch

class LoginViewModel (
    private val loginRepository: LoginRepository,
    private val context: Context
): ViewModel() {

    private var loginRole = ""
    private val username = MutableLiveData<String>()
    private val password = MutableLiveData<String>()

    private val doLogin = MutableLiveData<Boolean>()

    val loginData = MutableLiveData<Resource<String>>()
    val showroomData = MutableLiveData<Resource<Any>>()

    fun login(username: String, password: String) {
        this.username.value = username
        this.password.value = password
        //access api
        if (this.username.value.toString().contains("@")) {
            loginRole = SHFConstant.ROLE_USER
            verifyLogin()
        } else {
            loginRole = SHFConstant.ROLE_KOMITE
            verifyLDAP()
        }
    }

    private fun verifyLDAP() {
        viewModelScope.launch {
            // Make the network call and suspend execution until it finishes
            val ldapRequest = RequestUtil.createLdapRequest(username.value.toString(), password.value.toString())
            val result = loginRepository.loginLdap(ldapRequest)

            // Display result of the network request to the user
            if (result.status == Status.SUCCESS) {
                if (result.data is LdapBody) {
                    val response = result.data
                    if (response.loginResponseBody!!.succeed == "Berhasil Login") {
                        FirebaseCrashlytics.getInstance().setUserId(response.loginResponseBody!!.id.toString())
                        FirebaseCrashlytics.getInstance().setCustomKey("name_key", response.loginResponseBody!!.firstName.toString())


                        SessionManager.saveLoginData(
                            context,
                            username.value.toString(),
                            response.loginResponseBody!!.token,
                            response.loginResponseBody!!.id.toString(),
                            response.loginResponseBody!!.role,
                            response.loginResponseBody!!.firstName,
                            response.loginResponseBody!!.wilayah
                        )
                        loginData.value = Resource.success(loginRole)

                    } else {
                        throwErrorLogin("Username / Password salah")
                    }
                } else {
                    throwErrorLogin("Terjadi kesalahan dengan LDAP")
                }
            } else {
                if (result.message == "Not found")
                    throwErrorLogin("Username / Password salah")
                else throwErrorLogin("Error ${result.message}")
            }

        }
    }

    private fun verifyLogin(){
        viewModelScope.launch {
            val loginRequest = RequestUtil.createLoginRequest(username.value.toString(), password.value.toString())
            val result = loginRepository.login(loginRequest)

            if (result.status == Status.SUCCESS) {
                if (result.data is LoginResponseBody) {
                    val response = result.data
                    if (response.succeed == "Berhasil Login") {
                        //simpan preference
                        FirebaseCrashlytics.getInstance().setUserId(response.id.toString())
                        FirebaseCrashlytics.getInstance().setCustomKey("name_key", response.firstName.toString())


                        SessionManager.saveLoginData(
                            context,
                            username.value.toString(),
                            response.token,
                            response.id.toString(),
                            response.role,
                            response.firstName,
                            response.wilayah
                        )
                        loginData.value = Resource.success(loginRole)
                    } else {
                        throwErrorLogin("Username atau password salah")
                    }

                } else {
                    throwErrorLogin("Sukses tapi bukan Login ResponseBody")
                }
            } else {
                throwErrorLogin("Error ${result.message}")
            }
        }
    }

    private fun throwErrorLogin(errorMsg: String){
        loginData.value = Resource.error(errorMsg, null)
    }

    fun getShowroomData() {
        val token = SessionManager.getToken(context)!!
        viewModelScope.launch {
            showroomData.value = loginRepository.getShowroomDashboard(token)
        }
    }

}