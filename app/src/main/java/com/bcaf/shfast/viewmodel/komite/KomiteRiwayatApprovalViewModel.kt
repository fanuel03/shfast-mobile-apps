package com.bcaf.shfast.viewmodel.komite

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.bcaf.shfast.persistence.api.Resource
import com.bcaf.shfast.repository.KomiteRepository
import com.bcaf.shfast.util.SessionManager
import kotlinx.coroutines.launch

class KomiteRiwayatApprovalViewModel (
    private val komiteRepository: KomiteRepository,
    context: Context
): ViewModel()  {
    private var token = SessionManager.getToken(context)!!
    val historyData = MutableLiveData<Resource<Any>>()

    private var status = "Approved"
    fun setStatus (stat: String){
        this.status = stat
    }

    private val sortOptions = arrayOf("createdAt,desc", "createdAt,asc")
    private var sortType = 0
    fun setSort(type: Int) {
        this.sortType = type
    }

    fun refreshData() {

        viewModelScope.launch {
            historyData.value = komiteRepository.history(token, status, sortOptions[sortType])
        }
    }


}