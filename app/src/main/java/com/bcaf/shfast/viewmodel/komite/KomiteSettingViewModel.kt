package com.bcaf.shfast.viewmodel.komite

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.bcaf.shfast.persistence.api.Resource
import com.bcaf.shfast.repository.KomiteRepository
import com.bcaf.shfast.util.RequestUtil
import com.bcaf.shfast.util.SessionManager
import kotlinx.coroutines.launch

class KomiteSettingViewModel (
    private val komiteRepository: KomiteRepository,
    private val context: Context
): ViewModel()  {

    val logoutData = MutableLiveData<Resource<Any>>()

    fun logout() {
        val token = SessionManager.getToken(context)!!
        val username = SessionManager.getUsername(context)!!
        val body = RequestUtil.createLogoutRequest(username)

        viewModelScope.launch {
            logoutData.value = komiteRepository.logout(token, body)
        }
    }
}