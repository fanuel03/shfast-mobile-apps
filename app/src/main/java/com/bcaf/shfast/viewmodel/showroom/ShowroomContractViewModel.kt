package com.bcaf.shfast.viewmodel.showroom

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.bcaf.shfast.persistence.api.Resource
import com.bcaf.shfast.repository.ShowroomRepository
import com.bcaf.shfast.util.RequestUtil
import com.bcaf.shfast.util.SessionManager
import kotlinx.coroutines.launch

class ShowroomContractViewModel (
    private val showroomRepository: ShowroomRepository,
    private val context: Context
): ViewModel() {
    val token = SessionManager.getToken(context)!!
    val nik = SessionManager.getNik(context)
    val kontrakData = MutableLiveData<Resource<Any>>()

    fun refreshData(){
        if (nik.isNullOrBlank()) {
            kontrakData.value = Resource.error("Tidak ada NIK", null)
        } else {
            val body = RequestUtil.createListKontrakRequest(nik)
            viewModelScope.launch {
                kontrakData.value = showroomRepository.getListKontrak(token, body)
            }
        }
    }
}