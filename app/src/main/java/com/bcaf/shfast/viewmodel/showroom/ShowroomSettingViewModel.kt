package com.bcaf.shfast.viewmodel.showroom

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.bcaf.shfast.persistence.api.Resource
import com.bcaf.shfast.repository.ShowroomRepository
import com.bcaf.shfast.util.RequestUtil
import com.bcaf.shfast.util.SessionManager
import kotlinx.coroutines.launch

class ShowroomSettingViewModel (
    private val showroomRepository: ShowroomRepository,
    private val context: Context
): ViewModel() {

    val logoutData = MutableLiveData<Resource<Any>>()

    fun logout() {
        val token = SessionManager.getToken(context)!!
        val username = SessionManager.getUsername(context)!!
        val body = RequestUtil.createLogoutRequest(username)

        viewModelScope.launch {
            logoutData.value = showroomRepository.logout(token, body)
        }
    }

}