package com.bcaf.shfast.viewmodel.showroom

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.bcaf.shfast.ext.toIntString
import com.bcaf.shfast.persistence.api.Resource
import com.bcaf.shfast.repository.ShowroomRepository
import com.bcaf.shfast.util.RequestUtil
import com.bcaf.shfast.util.SessionManager
import kotlinx.coroutines.launch

class ShowroomContractDetailViewModel (
    private val showroomRepository: ShowroomRepository,
    private val context: Context
): ViewModel() {
    var noKontrak = ""
    var noPlat = ""
    var tanggal = ""


    val token = SessionManager.getToken(context)!!
    val simulasiData = MutableLiveData<Resource<Any>>()

    fun getSimulasi(takeBpkb: Boolean) {
        val norek = noKontrak.substring(0, 10)
        val nopin = noKontrak.substring(10)
        val request = RequestUtil.createSimulasiRequest(norek, nopin, tanggal, takeBpkb.toIntString())

        viewModelScope.launch {
            simulasiData.value = showroomRepository.getSimulasi(token, request!!)
        }
    }

}