package com.bcaf.shfast.viewmodel

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.bcaf.shfast.persistence.api.Resource
import com.bcaf.shfast.repository.SplashScreenRepository
import com.bcaf.shfast.util.SHFConstant
import com.bcaf.shfast.util.SessionManager
import kotlinx.coroutines.launch

class SplashScreenViewModel(
    private val splashScreenRepository: SplashScreenRepository,
    private val context: Context
): ViewModel() {

    internal val checkTokenData = MutableLiveData<Resource<Any>>()
    private var role: String? = null

    fun getRole() = role

    fun searchSession() {
        val id = SessionManager.getId(context)!!
        val token = SessionManager.getToken(context)!!
        role = SessionManager.getRole(context)
        when (role) {
            SHFConstant.ROLE_USER -> {
                viewModelScope.launch {
                    // Make the network call and suspend execution until it finishes
                    val result = splashScreenRepository.getShowroomDashboard(token)

                    // Display result of the network request to the user
                    checkTokenData.value = result
                }

            }
            SHFConstant.ROLE_CMO -> {
                checkTokenData.value = Resource.error("No session exist", null)
            }
            else -> {
                val wilayah = SessionManager.getWilayah(context)!!
                viewModelScope.launch {
                    if (token.isNullOrBlank()) {
                        checkTokenData.value = Resource.error("500", null)
                    } else {
                        // Make the network call and suspend execution until it finishes
                        val result = splashScreenRepository.getBucketKomite(token)

                        // Display result of the network request to the user
                        checkTokenData.value = result
                    }

                }

            }
        }
    }





}