package com.bcaf.shfast.viewmodel.showroom

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.bcaf.shfast.persistence.api.Resource
import com.bcaf.shfast.repository.ShowroomRepository
import com.bcaf.shfast.util.SessionManager
import kotlinx.coroutines.launch
import java.util.*

class ShowroomHistoryViewModel(
    private val showroomRepository: ShowroomRepository,
    private val context: Context
): ViewModel()  {

    private var token: String = SessionManager.getToken(context).toString()

    val historyData = MutableLiveData<Resource<Any>>()

    private var status = "Approved"
    fun setStatus (stat: String){
        this.status = stat
    }

    private val sortOptions = arrayOf("createdAt,desc", "createdAt,asc")
    private var sortType = 0
    fun setSort(type: Int) {
        this.sortType = type
    }

    fun refreshData() {
        viewModelScope.launch {
            historyData.value = showroomRepository.history(token,
                status.toLowerCase(Locale.ROOT), sortOptions[sortType])
        }
    }

}