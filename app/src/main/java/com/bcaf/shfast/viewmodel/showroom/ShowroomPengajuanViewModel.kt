package com.bcaf.shfast.viewmodel.showroom

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.bcaf.shfast.persistence.api.Resource
import com.bcaf.shfast.persistence.api.Status
import com.bcaf.shfast.persistence.model.Dokumen
import com.bcaf.shfast.persistence.response.showroom.UploadFileBody
import com.bcaf.shfast.repository.ShowroomRepository
import com.bcaf.shfast.util.FileUtil
import com.bcaf.shfast.util.RequestUtil
import com.bcaf.shfast.util.SessionManager
import com.google.firebase.crashlytics.FirebaseCrashlytics
import kotlinx.coroutines.launch
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.toRequestBody

class ShowroomPengajuanViewModel (
    private val showroomRepository: ShowroomRepository,
    private val context: Context
): ViewModel() {

    private val token = SessionManager.getToken(context)!!
    private val list: ArrayList<Dokumen> = ArrayList()
    private var otr: Int = 0
    private var nopol: String = ""
    private var noAkun: String = ""

    private val uuidData = MutableLiveData<String>()
    val uploadData = MutableLiveData<String>()
    val pengajuanData = MutableLiveData<Resource<Any>>()

    fun startPengajuan(list: ArrayList<Dokumen>, otr: Int, nopol: String, noAkun: String) {
        this.otr = otr
        this.nopol = nopol
        this.noAkun = noAkun
        this.list.apply {
            clear()
            addAll(list)
        }
        generateUuid()
    }

    private fun generateUuid() {
        viewModelScope.launch {
            uploadData.value = "Inisiasi Upload"

            if (uuidData.value.isNullOrBlank()) {
                val result =  showroomRepository.generateUuid(token)

                if (result.status == Status.SUCCESS && result.data is String) {
                    uuidData.value = result.data
                    uploadFile()
                } else {
                    pengajuanData.value = Resource.error("Gagal generate UUID", null)
                }
            } else {
                uploadFile()
            }
        }
    }

    private fun uploadFile() {
        val uuid = uuidData.value
        val showroomId = SessionManager.getId(context)
        var failUpload = false
        viewModelScope.launch loopDokumen@{
            list.forEach { dokumen ->
                uploadData.value = "Kompresi Gambar ${dokumen.name}"
                val fileName = "${dokumen.paramName}$uuid.jpg"
                val imageBytes = FileUtil.compress(dokumen.path!!)

                val requestBody = imageBytes.toRequestBody("image/jpeg".toMediaTypeOrNull(), 0, imageBytes.size)

                val image: MultipartBody.Part = MultipartBody.Part.createFormData("file", fileName, requestBody)

                uploadData.value = "Upload Gambar ${dokumen.name}"

                val result = showroomRepository.uploadFile(token, image)

                if (result.status == Status.SUCCESS && result.data is UploadFileBody) {
                    dokumen.downloadUri = result.data.fileDownloadUri
                } else {
                    pengajuanData.value = Resource.error("Gagal upload gambar ${dokumen.name}", null)
                    FirebaseCrashlytics.getInstance().log("ERROR UPLOAD ${dokumen.name} user $showroomId: ${result.message}")
                    failUpload = true
                    return@loopDokumen
                }
            }
            if(!failUpload) pengajuan()
        }

    }

    fun pengajuan() {
        val idShowroom = SessionManager.getId(context)!!
        viewModelScope.launch {
            val request = RequestUtil.createPengajuanRequest(uuidData.value!!, nopol, otr, idShowroom.toInt(), list, noAkun)

            val result = showroomRepository.pengajuan(token, request!!)
            pengajuanData.value = result
        }
    }

}