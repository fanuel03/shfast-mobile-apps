package com.bcaf.shfast.viewmodel.showroom

import androidx.lifecycle.MutableLiveData
import com.bcaf.shfast.persistence.api.Resource
import java.math.BigDecimal

class ShowroomListener  {
    val isSubmittingPengajuan = MutableLiveData<Resource<Any>>()
    val plafond = MutableLiveData<BigDecimal>()

    fun isSubmitting() {
        isSubmittingPengajuan.value = Resource.loading(null)
    }

    fun submittingDone(isSuccess: Boolean) {
        if (isSuccess) {
            isSubmittingPengajuan.value = Resource.success(null)
        } else {
            isSubmittingPengajuan.value = Resource.error("", null)
        }
    }

}