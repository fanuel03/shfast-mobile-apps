package com.bcaf.shfast.viewmodel.komite

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.bcaf.shfast.persistence.api.Resource
import com.bcaf.shfast.repository.KomiteRepository
import com.bcaf.shfast.util.RequestUtil
import com.bcaf.shfast.util.SessionManager
import kotlinx.coroutines.launch

class KomiteDetailRiwayatApprovalViewModel(
    private val komiteRepository: KomiteRepository,
    context: Context
): ViewModel()  {
    private var token: String = SessionManager.getToken(context).toString()
    val detailData = MutableLiveData<Resource<Any>>()

    fun loadData(idPenggunaan: String) {
        viewModelScope.launch {
            val body = RequestUtil.createDetaiPenggunaanRequest(idPenggunaan)
            detailData.value = komiteRepository.getDetailHistory(token, body!!)
        }
    }
}