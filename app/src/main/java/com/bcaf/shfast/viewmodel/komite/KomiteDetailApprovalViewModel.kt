package com.bcaf.shfast.viewmodel.komite

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.bcaf.shfast.persistence.api.Resource
import com.bcaf.shfast.repository.KomiteRepository
import com.bcaf.shfast.util.RequestUtil
import com.bcaf.shfast.util.SessionManager
import kotlinx.coroutines.launch

class KomiteDetailApprovalViewModel(
    private val komiteRepository: KomiteRepository,
    context: Context
): ViewModel() {
    private val token = SessionManager.getToken(context)!!

    val detailData = MutableLiveData<Resource<Any>>()
    val approvalData = MutableLiveData<Resource<Any>>()
    var isApproved = true
    var idShowroom = "-1"

    fun loadData(idPenggunaan: String) {
        viewModelScope.launch {
            val body = RequestUtil.createDetaiPenggunaanRequest(idPenggunaan)
            detailData.value = komiteRepository.getDetailBucket(token, body!!)
        }
    }

    fun submitData(idPenggunaan: String, otr: String, reason: String, status: String) {
        if (status == "Rejected") isApproved = false
        viewModelScope.launch {
            if (status == "Rejected") {
                val body = RequestUtil.createRejectRequest(idPenggunaan, reason, idShowroom)

                approvalData.value = komiteRepository.putReject(token, body!!)
            } else {
                val body = RequestUtil.createApprovalRequest(idPenggunaan, otr, reason, status)

                approvalData.value = komiteRepository.putApproval(token, body!!)

            }
        }
    }
}