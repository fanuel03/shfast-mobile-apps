package com.bcaf.shfast.viewmodel.showroom

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.bcaf.shfast.persistence.api.Resource
import com.bcaf.shfast.repository.ShowroomRepository
import com.bcaf.shfast.util.RequestUtil
import com.bcaf.shfast.util.SessionManager
import kotlinx.coroutines.launch

class ShowroomHistoryDetailViewModel(
    private val showroomRepository: ShowroomRepository,
    private val context: Context
): ViewModel()  {

    private val token = SessionManager.getToken(context)!!

    private lateinit var idPengajuan: String
    private lateinit var otr: String
    val detailData = MutableLiveData<Resource<Any>>()
    val cancelData = MutableLiveData<Resource<Any>>()

    fun setOtr(otr: String) {
        this.otr = otr
    }

    fun setIdPengajuan(id: String) {
        this.idPengajuan = id
    }

    fun fetchDetail(id: String) {
        viewModelScope.launch {
            val body = RequestUtil.createDetaiPenggunaanRequest(id)
            detailData.value = showroomRepository.detail(token, body!!)
        }
    }

    fun cancelRequest() {
        val body = RequestUtil.createCancelRequest(idPengajuan)!!
        viewModelScope.launch {
            cancelData.value = showroomRepository.cancel(token, body)
        }
    }
}