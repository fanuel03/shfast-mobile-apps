package com.bcaf.shfast.viewmodel.showroom

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.bcaf.shfast.persistence.api.Resource
import com.bcaf.shfast.repository.ShowroomRepository
import com.bcaf.shfast.util.SessionManager
import kotlinx.coroutines.launch

class ShowroomDashboardViewModel(
    private val showroomRepository: ShowroomRepository,
    private val context: Context
): ViewModel() {
    val token = SessionManager.getToken(context)!!

    val dashboardData = MutableLiveData<Resource<Any>>()
    init {
        refreshData()
    }

    fun refreshData() {
        viewModelScope.launch {
            dashboardData.value =  showroomRepository.getShowroomDashboard(token)
        }
    }
}