package com.bcaf.shfast.viewmodel.showroom

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.bcaf.shfast.persistence.api.Resource
import com.bcaf.shfast.repository.ShowroomRepository
import com.bcaf.shfast.util.RequestUtil
import com.bcaf.shfast.util.SessionManager
import kotlinx.coroutines.launch

class ShowroomChangePasswordViewModel(
    private val showroomRepository: ShowroomRepository,
    private val context: Context
): ViewModel() {

    val passwordData = MutableLiveData<Resource<Any>>()

    fun changePassword(oldPassword: String, newPassword: String) {
        viewModelScope.launch {
            val token = SessionManager.getToken(context)
            val requestBody = RequestUtil.createChangePassword(oldPassword, newPassword)

            passwordData.value = showroomRepository.changePassword(token!!, requestBody!!)
        }
    }
}