package com.bcaf.shfast.viewmodel.komite

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.bcaf.shfast.persistence.api.Resource
import com.bcaf.shfast.repository.KomiteRepository
import com.bcaf.shfast.util.SessionManager
import kotlinx.coroutines.launch

class KomiteBucketApprovalViewModel(
    private val komiteRepository: KomiteRepository,
    context: Context
): ViewModel()  {
    private var token: String = SessionManager.getToken(context).toString()

    val bucketData = MutableLiveData<Resource<Any>>()

    fun refreshData() {
        viewModelScope.launch {
            bucketData.value = komiteRepository.getBucketKomite(token)
        }
    }
}