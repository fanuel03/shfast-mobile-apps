package com.bcaf.shfast.ui

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.bcaf.shfast.R
import kotlinx.android.synthetic.main.activity_web.*

class WebViewActivity: AppCompatActivity() {
    companion object {
        private const val EXTRA_URL = "EXTRA_URL"
        private const val EXTRA_TITLE = "EXTRA_TITLE"
        @JvmStatic
        fun getIntent(context: Context, title: String, url: String): Intent {
            return Intent(context, WebViewActivity::class.java).apply {
                putExtra(EXTRA_URL, url)
                putExtra(EXTRA_TITLE, title)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_web)
        supportActionBar!!.hide()

        headerWebview.text = intent.getStringExtra(EXTRA_TITLE) ?: "Webview SHFast"

        val url = intent.getStringExtra(EXTRA_URL)
        if (url == null) finish()
        initEvent(url!!)
    }

    private fun initEvent(url: String) {
        val ws = webView.settings
        ws.javaScriptEnabled = true
        ws.displayZoomControls = false
        ws.builtInZoomControls = true
        ws.allowFileAccessFromFileURLs = true
        ws.loadWithOverviewMode = true
        ws.useWideViewPort = true
        ws.allowContentAccess = true
        webView.loadUrl(url)

        btnBackWebview.setOnClickListener { finish() }

        btnRefreshWebview.setOnClickListener {
            webView.reload()
        }
    }

}