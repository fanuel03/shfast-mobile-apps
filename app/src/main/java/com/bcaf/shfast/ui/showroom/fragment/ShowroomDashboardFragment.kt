package com.bcaf.shfast.ui.showroom.fragment

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.bcaf.shfast.R
import com.bcaf.shfast.ext.SHFFragment
import com.bcaf.shfast.ext.toRupiah
import com.bcaf.shfast.persistence.api.Status
import com.bcaf.shfast.persistence.model.Pengajuan
import com.bcaf.shfast.persistence.response.showroom.ShowroomDashboardBody
import com.bcaf.shfast.recyclerview.adapter.PengajuanAdapter
import com.bcaf.shfast.recyclerview.callback.DetailCallback
import com.bcaf.shfast.ui.showroom.ShowroomDetailPengajuanActivity
import com.bcaf.shfast.util.SHFUtil
import com.bcaf.shfast.util.SessionManager
import com.bcaf.shfast.viewmodel.showroom.ShowroomDashboardViewModel
import com.bcaf.shfast.viewmodel.showroom.ShowroomListener
import kotlinx.android.synthetic.main.fragment_dashboard_showroom.*
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.viewModel
import java.util.*
import kotlin.collections.ArrayList

class ShowroomDashboardFragment: SHFFragment() {
    private val listener: ShowroomListener by inject()
    private val viewModel: ShowroomDashboardViewModel by viewModel()
    private var adapter: PengajuanAdapter? = null
    private val list: ArrayList<Pengajuan> = ArrayList()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.fragment_dashboard_showroom, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initProfile()
        isOnLoad()
        initRv()
        observe()
    }

    override fun refreshData() {
        isOnLoad()
        viewModel.refreshData()
    }

    private fun initProfile() {
        nama_owner.text = SessionManager.getNama(context!!)
        jabatan.text = "Owner"
        nama_showroom.text = SessionManager.getWilayah(context!!)
    }

    private fun isOnLoad(){
        error_text.visibility = View.GONE
        loading_rv_pending.visibility = View.VISIBLE
        rv_container.visibility = View.GONE
    }

    private fun stopLoad(isDataExist: Boolean = false) {
        loading_rv_pending.visibility = View.GONE
        if (isDataExist) {
            rv_container.visibility = View.VISIBLE
        } else {
            error_text.visibility = View.VISIBLE
        }
    }

    private fun initRv() {
        if (adapter == null) {
            rv_pending.isNestedScrollingEnabled = false
            adapter = PengajuanAdapter(list, context!!, object: DetailCallback {
                override fun onClick(id: String) {
                    startActivity(
                        ShowroomDetailPengajuanActivity.getIntent(
                            this@ShowroomDashboardFragment.context!!,
                            id
                        )
                    )
                }
            })
            rv_pending.layoutManager = LinearLayoutManager(context!!, LinearLayoutManager.VERTICAL, false)
            rv_pending.adapter = adapter
        }
    }

    private fun observe() {
        viewModel.dashboardData.observe(this, Observer {
            if (it.status == Status.SUCCESS) {
                if (it.data is ShowroomDashboardBody) {
                    val response = it.data

                    SessionManager.setNIK(context!!, response.nik!!)
                    SessionManager.setTanggalExpired(context!!, it.data.masaBerlaku!!)

                    // Set Norek
                    val listRekening = response.noRekening
                    val indexOfDefaultRekening = listRekening?.indexOfFirst { rek-> rek.defaultSts == "1" }
                    if (indexOfDefaultRekening != null && indexOfDefaultRekening != 0) {
                        Collections.swap(listRekening, indexOfDefaultRekening, 0)
                    }
                    SessionManager.saveNorek(activity!!, listRekening ?: arrayListOf())
                    listener.plafond.value = response.plafond!!.toInt().toBigDecimal()

                    plafond.text = response.plafond!!.toLong().toRupiah()
                    exposure.text = response.exposure!!.toLong().toRupiah()
                    jumlahAkun.text = response.jumlahAkunAktif!!.toInt().toString()
                    pengajuanPending.text = response.jumlahAccount!!.toString()
                    tglExpired.text = "Berlaku Sampai " + SHFUtil.parseTime(response.masaBerlaku!!)

                    val listPending = response.historyPenggunaanShf!!
                    stopLoad(listPending.isNotEmpty())
                    listPending.forEach { historyPengajuan ->
                        list.add(historyPengajuan.toPengajuan())
                    }
                    updateData()


                } else {
                    Log.e("DASHBOARD", "Wrong Type: ${it.message.toString()}")
                    stopLoad()
                    error_text.text = getString(R.string.failed_please_reload)
                }
            } else {
                Log.e("DASHBOARD", it.message.toString())
                stopLoad()
                error_text.text = getString(R.string.failed_please_reload)
            }
        })
    }

    private fun updateData(){
        adapter?.notifyDataSetChanged()
    }

}