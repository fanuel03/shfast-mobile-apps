package com.bcaf.shfast.ui.showroom.fragment

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.bcaf.shfast.R
import com.bcaf.shfast.ext.SHFFragment
import com.bcaf.shfast.persistence.api.Status
import com.bcaf.shfast.persistence.model.Kontrak
import com.bcaf.shfast.persistence.response.showroom.ListKontrakBody
import com.bcaf.shfast.recyclerview.adapter.ContractAdapter
import com.bcaf.shfast.recyclerview.callback.ContractCallback
import com.bcaf.shfast.ui.showroom.ShowroomDetailContractActivity
import com.bcaf.shfast.util.SessionManager
import com.bcaf.shfast.viewmodel.showroom.ShowroomContractViewModel
import com.bumptech.glide.Glide
import com.google.firebase.crashlytics.FirebaseCrashlytics
import kotlinx.android.synthetic.main.fragment_showroom_contract.*
import org.koin.android.viewmodel.ext.android.viewModel


class ShowroomContractFragment: SHFFragment() {

    private var adapter: ContractAdapter? = null
    private val viewModel: ShowroomContractViewModel by viewModel()

    private val list: ArrayList<Kontrak> = ArrayList()


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_showroom_contract, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Glide.with(this).load(R.drawable.loading).into(imgLoading)

        initEvent()
        initRv()
        observe()

        if (SessionManager.getNik(context!!).isNullOrBlank()) {
            showErrorView(isDataEmpty = false)
            error_header.text = "NIK Belum terdaftar"
            btn_refresh.visibility = View.GONE
            loading_rv_contract.visibility = View.GONE
        } else {
            refresh()
        }

    }

    private fun initEvent() {
        btn_refresh.setOnClickListener {
            refresh()
        }
    }

    private  fun initRv() {
        if (adapter == null) {
            adapter = ContractAdapter(list, context!!, object: ContractCallback {
                override fun onClick(kontrak: Kontrak) {
                    startActivity(ShowroomDetailContractActivity.getIntent(context!!, kontrak.noKontrak!!, kontrak.noPolisi!!))
                }
            })

            rv_contract.layoutManager = LinearLayoutManager(context!!, LinearLayoutManager.VERTICAL, false)
            rv_contract.adapter = adapter
        }
    }

    private fun observe() {
        viewModel.kontrakData.observe(this, Observer { it ->
            loading_rv_contract.visibility = View.GONE
            if (it.status == Status.SUCCESS) {
                if (it.data is ListKontrakBody) {
                    val response: ListKontrakBody = it.data
                    val finaHeader = response.responseHeader
                    if(response.kontrakList?.kontrak != null && response.kontrakList.kontrak!!.isNotEmpty()) {
//                    if (finaHeader != null || response.kontrakList != null) {
                        val listKontrak = response.kontrakList.kontrak!!
                        if (listKontrak.isEmpty()) {
                            showErrorView(isDataEmpty = true)
                        } else {
                            val newList = listKontrak.filter { kontrak-> kontrak.tglClosed.isNullOrBlank() }
                            list.addAll(newList)
                            adapter?.notifyDataSetChanged()
                            rv_container.visibility = View.VISIBLE
                        }
                        if (true) {
//                        if (finaHeader.errorCode == "0000") {

                        } else {
                            FirebaseCrashlytics.getInstance().recordException(Exception(finaHeader?.errorDesc))
                            Log.e("LIST_CONTRACT", finaHeader?.errorDesc.toString())
                            showErrorView(isDataEmpty = false)
                        }
                    } else {
                        FirebaseCrashlytics.getInstance().recordException(Exception("Header is null"))
                        Log.e("LIST_CONTRACT", "Header is null")
                        showErrorView(isDataEmpty = false)
                    }


                } else {
                    Log.e("LIST_CONTRACT", "Object not compatible")
                }
            } else {
                Log.e("LIST_CONTRACT", it.message.toString())
                showErrorView(isDataEmpty = false)
            }
        })
    }

    private fun refresh() {
        list.clear()

        rv_container.visibility = View.GONE
        error_container.visibility = View.GONE
        loading_rv_contract.visibility = View.VISIBLE
        viewModel.refreshData()
    }

    private fun showErrorView(isDataEmpty: Boolean) {
        rv_container.visibility = View.GONE
        error_container.visibility = View.VISIBLE
        if (isDataEmpty) {
            error_image.visibility = View.GONE
            error_header.text = "Belum ada pinjaman berjalan"
        } else {
            error_image.visibility = View.VISIBLE
            error_header.text = "Terjadi kesalahan"
        }
    }

    override fun refreshData() {}
}