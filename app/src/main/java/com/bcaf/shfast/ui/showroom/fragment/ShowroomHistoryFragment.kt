package com.bcaf.shfast.ui.showroom.fragment

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.bcaf.shfast.R
import com.bcaf.shfast.ext.SHFFragment
import com.bcaf.shfast.persistence.api.Status
import com.bcaf.shfast.persistence.model.Pengajuan
import com.bcaf.shfast.persistence.response.showroom.HistoryPengajuanBody
import com.bcaf.shfast.recyclerview.adapter.PengajuanAdapter
import com.bcaf.shfast.recyclerview.callback.DetailCallback
import com.bcaf.shfast.ui.showroom.ShowroomDetailPengajuanActivity
import com.bcaf.shfast.viewmodel.showroom.ShowroomHistoryViewModel
import com.bumptech.glide.Glide
import com.google.android.material.chip.Chip
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import kotlinx.android.synthetic.main.fragment_showroom_history.*
import org.koin.android.viewmodel.ext.android.viewModel
import java.util.*
import kotlin.collections.ArrayList
import kotlin.concurrent.schedule

class ShowroomHistoryFragment: SHFFragment() {
    private var adapter: PengajuanAdapter? = null
    private val viewModel: ShowroomHistoryViewModel by viewModel()

    private val list: ArrayList<Pengajuan> = ArrayList()

    private val sortOptions = arrayOf("Tanggal Terbaru", "Tanggal Terlama")
    private var checkedItem = 0
    private var choosenItem = 0

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_showroom_history, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Glide.with(this).load(R.drawable.loading).into(imgLoading)

        initEvent()
        initRv()
        observe()

        refresh()
    }

    private fun refresh() {
        list.clear()
        rv_container.visibility = View.GONE
        error_container.visibility = View.GONE
        loading_rv_history.visibility = View.VISIBLE

        Timer("Setup", false).schedule(1000) {
            viewModel.refreshData()
        }

    }


    private fun initEvent() {

        chip_group.setOnCheckedChangeListener { _, checkedId ->
            val chip = chip_group.findViewById<Chip>(checkedId)
            chip?.let {
                viewModel.setStatus(it.text.toString())
                refresh()
            }
        }

        filter.setOnClickListener {
            MaterialAlertDialogBuilder(context!!)
                .setTitle("Urutkan berdasarkan:")
                .setNeutralButton(resources.getString(R.string.cancel)) { dialog, _ ->
                    dialog.dismiss()
                }
                .setPositiveButton(resources.getString(R.string.ok)) { dialog, _ ->
                    checkedItem = choosenItem
                    viewModel.setSort(checkedItem)
                    refresh()
                    dialog.dismiss()
                }
                // Single-choice items (initialized with checked item)
                .setSingleChoiceItems(sortOptions, checkedItem) { _, which ->
                    choosenItem = which
                }
                .show()
        }

        btn_refresh.setOnClickListener {
            refresh()
        }
    }

    private fun initRv() {
        if (adapter == null) {
            rv_history.isNestedScrollingEnabled = false
            adapter = PengajuanAdapter(list, context!!, object: DetailCallback {
                override fun onClick(id: String) {
                    startActivityForResult(
                        ShowroomDetailPengajuanActivity.getIntent(
                            this@ShowroomHistoryFragment.context!!,
                            id
                        ),
                        ShowroomDetailPengajuanActivity.DETAIL_REQUEST_ID
                    )
                }
            })

            rv_history.layoutManager = LinearLayoutManager(context!!, LinearLayoutManager.VERTICAL, false)
            rv_history.adapter = adapter
        }
    }

    private fun observe(){
        viewModel.historyData.observe(this, Observer {
            loading_rv_history.visibility = View.GONE
            if (it.status == Status.SUCCESS) {
                if (it.data is HistoryPengajuanBody) {
                    val response = it.data
                    if (response.listHistoryPenggunaan == null  || response.listHistoryPenggunaan?.isEmpty()!!) {
                        showErrorView(isDataEmpty = true)
                    } else {
                        response.listHistoryPenggunaan!!.forEach { history->
                            list.add(history.toPengajuan())
                        }
                        adapter?.notifyDataSetChanged()
                        rv_container.visibility = View.VISIBLE
                    }
                }
            } else {
                showErrorView(isDataEmpty = false)
            }
        })
    }

    private fun showErrorView(isDataEmpty: Boolean) {
        rv_container.visibility = View.GONE
        error_container.visibility = View.VISIBLE
        if (isDataEmpty) {
            error_image.visibility = View.GONE
            error_header.text = "Belum ada pengajuan"
            error_desc.visibility = View.VISIBLE
            image_point_down.visibility = View.VISIBLE
        } else {
            error_image.visibility = View.VISIBLE
            error_header.text = "Terjadi kesalahan"
            error_desc.visibility = View.GONE
            image_point_down.visibility = View.GONE
        }
    }


    override fun refreshData() {}

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == ShowroomDetailPengajuanActivity.DETAIL_REQUEST_ID) {
            if (resultCode == Activity.RESULT_OK) {
                refresh()
            }
        }
    }
}