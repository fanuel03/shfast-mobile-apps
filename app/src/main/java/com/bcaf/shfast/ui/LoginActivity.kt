package com.bcaf.shfast.ui

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.bcaf.shfast.R
import com.bcaf.shfast.persistence.api.Status
import com.bcaf.shfast.persistence.response.showroom.ShowroomDashboardBody
import com.bcaf.shfast.ui.komite.KomiteDashboardActivity
import com.bcaf.shfast.ui.showroom.ShowroomDashboardActivity
import com.bcaf.shfast.util.SHFConstant
import com.bcaf.shfast.util.SHFUtil
import com.bcaf.shfast.util.SessionManager
import com.bcaf.shfast.viewmodel.LoginViewModel
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.android.viewmodel.ext.android.viewModel

class LoginActivity: AppCompatActivity() {
    private val viewModel: LoginViewModel by viewModel()
    private var doubleBackToExitPressedOnce = false

    companion object {
        @JvmStatic
        fun getIntent(
            context: Context
        ): Intent {
            return Intent(context, LoginActivity::class.java)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        setContentView(R.layout.activity_main)
        supportActionBar!!.hide()
        initEvent()
        observe()
        //throw RuntimeException("Test Crash")

    }

    private fun initEvent(){
        Glide.with(this).load(R.drawable.loading).into(imgLoading)

        lupa_password.visibility = View.GONE

        ig.setOnClickListener { openUrl("https://www.instagram.com/mybcaf/") }
        web.setOnClickListener { openUrl("https://www.bcafinance.co.id/") }
        sign_In.setOnClickListener {
            doSignIn()
        }
    }

    private fun observe(){
        viewModel.loginData.observe(this, Observer {

            if (it.status == Status.SUCCESS) {
                if (it.data is String) {
                    if (it.data == SHFConstant.ROLE_USER) {
                       viewModel.getShowroomData()
                    } else if (it.data != SHFConstant.ROLE_CMO) {
                        stopLoading()
                        startActivity(KomiteDashboardActivity.getIntent(this).apply {
                            flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                        })
                    }
                } else {
                    stopLoading()
                    Log.e("LOGIN", it.data.toString())
                    Toast.makeText(this, "Terjadi kesalahan pengambilan data", Toast.LENGTH_LONG).show()
                }
            } else {
                stopLoading()
                Toast.makeText(this, it.message.toString(), Toast.LENGTH_LONG).show()
            }
        })

        viewModel.showroomData.observe(this, Observer {
            if (it.status == Status.SUCCESS) {
                if (it.data is ShowroomDashboardBody) {
                    SessionManager.setTanggalExpired(this, it.data.masaBerlaku!!)
                }
            }
            stopLoading()
            startActivity(ShowroomDashboardActivity.getIntent(this).apply {
                flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            })
        })
    }

    private fun openUrl(url: String) {
        val i = Intent(Intent.ACTION_VIEW).apply {
            data = Uri.parse(url)
        }
        startActivities(arrayOf(i))
    }

    private fun doSignIn() {

        if(inputValid()) {
            loading()
            viewModel.login(email.text.toString(), password.text.toString())
        } else {
            Toast.makeText(this, "Invalid Username or Email", Toast.LENGTH_SHORT).show()
        }
    }

    private fun inputValid(): Boolean {
        return if (!(email.text.toString().isEmpty() || password.text.toString().isEmpty())) {
            SHFUtil.isEmailValid(email.text.toString())
        } else {
            false
        }
    }

    private fun loading() {
        loading_login.visibility = View.VISIBLE
    }

    private fun stopLoading() {
        loading_login.visibility = View.GONE
    }

    override fun onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed()
            return
        }

        this.doubleBackToExitPressedOnce = true
        Toast.makeText(this, "Tekan Kembali sekali lagi untuk keluar", Toast.LENGTH_SHORT).show()

        Handler().postDelayed(Runnable { doubleBackToExitPressedOnce = false }, 2000)
    }

}