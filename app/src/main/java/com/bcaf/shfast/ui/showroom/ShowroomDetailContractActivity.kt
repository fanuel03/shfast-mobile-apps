package com.bcaf.shfast.ui.showroom

import android.app.DatePickerDialog
import android.app.DatePickerDialog.OnDateSetListener
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.bcaf.shfast.R
import com.bcaf.shfast.ext.toRupiah
import com.bcaf.shfast.persistence.api.Status
import com.bcaf.shfast.persistence.response.showroom.simulasi.SimulasiETBody
import com.bcaf.shfast.util.DateUtil
import com.bcaf.shfast.util.SHFUtil
import com.bcaf.shfast.viewmodel.showroom.ShowroomContractDetailViewModel
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_detail_kontrak_showroom.*
import org.koin.android.viewmodel.ext.android.viewModel
import java.util.*

class ShowroomDetailContractActivity: AppCompatActivity() {
    private val viewModel: ShowroomContractDetailViewModel by viewModel()

    companion object {
        private const val EXTRA_PLAT = "EXTRA_PLAT"
        private const val EXTRA_KONTRAK = "EXTRA_KONTRAK"


        @JvmStatic
        fun getIntent(context: Context, noKontrak: String, noPlat: String): Intent {
            return Intent(context, ShowroomDetailContractActivity::class.java).apply {
                putExtra(EXTRA_PLAT, noPlat)
                putExtra(EXTRA_KONTRAK, noKontrak)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_kontrak_showroom)
        supportActionBar!!.title = "Detail Pengajuan"
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        viewModel.noKontrak = intent.getSerializableExtra(EXTRA_KONTRAK) as String
        viewModel.noPlat = intent.getSerializableExtra(EXTRA_PLAT) as String

        initEvent()
        observe()
    }

    private fun initEvent() {
        Glide.with(this).load(R.drawable.loading).into(imgLoading)

        noPlat.text = viewModel.noPlat
        noKontrak.text = viewModel.noKontrak

        btnCalendar.setOnClickListener {
            val calendar = Calendar.getInstance()
            var year = 0
            var month = 0
            var day = 0

            if (viewModel.tanggal.isBlank()) {
                year = calendar.get(Calendar.YEAR)
                month = calendar.get(Calendar.MONTH)
                day = calendar.get(Calendar.DATE)
            } else {
                val arrExtractDate = DateUtil.extractDate(viewModel.tanggal)
                year = arrExtractDate[2]
                month = arrExtractDate[1]
                day = arrExtractDate[0]
            }

            val datePicker = DatePickerDialog(
                this,
                R.style.CustomDatePicker,
                OnDateSetListener { datePicker, sYear, sMonth, dayOfMonth ->
                    val selectedDate = Calendar.getInstance()
                    selectedDate[sYear, sMonth] = dayOfMonth
                    // SET TANGGAL
                    viewModel.tanggal = SHFUtil.formatTime(selectedDate)!!
                    tglPelunasan.text = viewModel.tanggal
                    btnHitung.visibility = View.VISIBLE
                },
                year,
                month,
                day
            )
            datePicker.datePicker.minDate = calendar.time.time
            datePicker.show()
        }

        btnHitung.setOnClickListener {
            if (viewModel.tanggal.isNotBlank()) {
                loading_detail_et.visibility = View.VISIBLE
                viewModel.getSimulasi(ambilBPKB.isChecked)
            } else {
                Toast.makeText(this, "Silahkan pilih tanggal pelunasan", Toast.LENGTH_LONG).show()
            }
        }
    }

    private fun observe() {
        viewModel.simulasiData.observe(this, Observer {
            if (it.status == Status.SUCCESS) {
                if (it.data is SimulasiETBody) {

                    val response = it.data.etshFastResponse!!

                    response.cleanseData()

                    //noVA.setText(temp_data.getVirtualAccountNo()+"");
                    osPH.text = response.pokokHutang.toRupiah()
                    admin.text = response.biayaAdmin.toRupiah()
                    bunga.text = response.bunga.toRupiah()
                    denda.text = response.denda.toRupiah()
                    totPelunasan.text = response.totalSimulasiET.toRupiah()

                    loading_detail_et.visibility = View.GONE
                    container_detail_et.visibility = View.VISIBLE
                } else {
                    showError()
                }
            } else {
                showError()
            }
        })
    }

    private fun showError() {
        loading_detail_et.visibility = View.GONE
        container_error_et.visibility = View.VISIBLE
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                finish()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

}