package com.bcaf.shfast.ui.komite

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.bcaf.shfast.BuildConfig
import com.bcaf.shfast.R
import com.bcaf.shfast.persistence.api.Status
import com.bcaf.shfast.persistence.response.SHFDefaultResponse
import com.bcaf.shfast.persistence.response.komite.DetailPengajuanKomite
import com.bcaf.shfast.persistence.response.komite.StatusRekomendasi
import com.bcaf.shfast.recyclerview.adapter.TrackingAdapter
import com.bcaf.shfast.ui.WebViewActivity
import com.bcaf.shfast.util.SHFUtil
import com.bcaf.shfast.viewmodel.komite.KomiteDetailApprovalViewModel
import com.bumptech.glide.Glide
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import kotlinx.android.synthetic.main.activity_detail_pengajuan_komite.*
import org.koin.android.viewmodel.ext.android.viewModel
import java.math.BigDecimal
import java.math.RoundingMode
import java.util.*

class KomiteDetailPengajuanActivity: AppCompatActivity() {

    private val viewModel: KomiteDetailApprovalViewModel by viewModel()
    private lateinit var idPenggunaan: String
    private var currentOtr = "Rp.0"

    private lateinit var otr: BigDecimal
    private var adapter: TrackingAdapter? = null

    companion object {
        const val REQUEST_APPROVAL = 4490
        private const val EXTRA_ID_PENGAJUAN = "EXTRA_ID_PENGAJUAN"
        private const val BASE_URL = BuildConfig.BASE_URL
        @JvmStatic
        fun getIntent(context: Context, idPenggunaan: String): Intent {
            return Intent(context, KomiteDetailPengajuanActivity::class.java).apply {
                putExtra(EXTRA_ID_PENGAJUAN, idPenggunaan)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_pengajuan_komite)
        supportActionBar!!.hide()

        idPenggunaan = intent.getStringExtra(EXTRA_ID_PENGAJUAN)!!

        initEvent()
        observe()

        containerLoadingDetailKomite.visibility = View.VISIBLE
        viewModel.loadData(idPenggunaan)
    }

    private fun initEvent() {
        Glide.with(this).load(R.drawable.loading).into(imgLoading)

        btnBackApprovalKomite.setOnClickListener {
            finish()
        }

        btnApprove.setOnClickListener {
            alertApprove()
        }

        btnReject.setOnClickListener {
            alertReject()
        }

        btnAwc.setOnClickListener {
            alertAwc()
        }
    }

    private fun observe() {
        viewModel.detailData.observe(this, Observer {
            containerLoadingDetailKomite.visibility = View.GONE

            if (it.status == Status.SUCCESS) {
                if (it.data is DetailPengajuanKomite) {
                    val response = it.data

                    viewModel.idShowroom = response.idShowroom!!

                    txtNamaShowroom.text = response.namaShowroom ?: "-"
                    txtWilayahShowroom.text = response.wilayahShowroom ?: "-"
                    txtLoanOtr.text = "Rp. "+SHFUtil.rupiahFormat(response.jumlahPenggunaan?.toBigDecimal()?.setScale(0, RoundingMode.HALF_EVEN)!!)
                    txtLoadPh.text = "Rp." +SHFUtil.rupiahFormat((response.jumlahPenggunaan?.toBigDecimal()!!.multiply(BigDecimal(0.6))).setScale(0,RoundingMode.HALF_EVEN))
                    txtVehicleTipe.text = response.type ?: "-"
                    txtVehicleBrand.text = response.merk ?: "-"
                    txtVehicleLicense.text = response.noPol ?: "-"
                    txtVehicleReqDate.text = SHFUtil.parseTime(response.createdAt!!)
                    txtVehicleYear.text = response.year ?: "-"

                    handleTracking(response.statusRekomendasi)

                    otr = response.statusRekomendasi?.lastOrNull { sts -> sts.status == "Awc" }?.let { stsRek ->
                        stsRek.otrRekomen!!.toBigDecimal().setScale(0, RoundingMode.HALF_EVEN)
                    } ?: response.jumlahPenggunaan.toBigDecimal().setScale(0, RoundingMode.HALF_EVEN)

                    //otr = response.jumlahPenggunaan.toBigDecimal().setScale(0, RoundingMode.HALF_EVEN)

                    btnDokumenBpkb.setOnClickListener {
                        startActivity(WebViewActivity.getIntent(this, "BPKB",BASE_URL + response.fotoBPKB))
                    }

                    btnDokumenStnk.setOnClickListener {
                        startActivity(WebViewActivity.getIntent(this, "STNK", BASE_URL + response.fotoSTNK))
                    }

                    btnDokumenFaktur.setOnClickListener {
                        startActivity(WebViewActivity.getIntent(this, "Faktur",BASE_URL + response.fotoFaktur))
                    }

                    btnDokumenFoken.setOnClickListener {
                        startActivity(WebViewActivity.getIntent(this, "Foto Kendaraan",BASE_URL + response.fotoTampakDepan))
                    }

                    btnOlx.setOnClickListener {
                        startActivity(WebViewActivity.getIntent(this, "OLX", SHFUtil.buildUrlOLX(response.merk ?: "", response.type ?: "")))
                    }

                    btnCarmudi.setOnClickListener {
                        startActivity(WebViewActivity.getIntent(this, "Carmudi",SHFUtil.buildUrlCarmudi(response.merk ?: "", response.type ?: "")))
                    }


                } else {
                    Toast.makeText(this, "Terjadi kesalahan ketika mengambil data", Toast.LENGTH_LONG).show()
                }
            } else {
                Toast.makeText(this, "Terjadi kesalahan ketika mengambil data", Toast.LENGTH_LONG).show()
            }
        })

        viewModel.approvalData.observe(this, Observer {
            containerLoadingDetailKomite.visibility = View.GONE

            if (it.status == Status.SUCCESS) {
                if (it.data is SHFDefaultResponse) {
                    val response = it.data
                    if (response.status!!.toLowerCase(Locale.ROOT) == "success") {
                        alertResponse()
                    } else {
                        Toast.makeText(this, "Terjadi Kesalahan ,silahkan coba kembali", Toast.LENGTH_LONG).show()
                    }
                } else {
                    Toast.makeText(this, "Terjadi Kesalahan ,silahkan coba kembali", Toast.LENGTH_LONG).show()

                }
            } else {
                Toast.makeText(this, "Terjadi Kesalahan ,silahkan coba kembali", Toast.LENGTH_LONG).show()
            }

        })
    }

    private fun alertApprove() {
        MaterialAlertDialogBuilder(this)
            .setTitle("Approval Pengajuan")
            .setMessage("Apakah anda menyetujui pengajuan ini ?")
            .setPositiveButton("OK") { dialog, _ ->
                dialog.dismiss()
                containerLoadingDetailKomite.visibility = View.VISIBLE
                viewModel.submitData(idPenggunaan, otr.toString(), "Approved", "Approved")
            }.setNegativeButton("Cancel") { dialog, _ ->
                dialog.dismiss()
            }.show()
    }

    private fun alertReject() {
        val alertView = layoutInflater.inflate(R.layout.alert_reject, null)

        val txtReject = alertView.findViewById<EditText>(R.id.txtRejectReason)
        val txtReason =  alertView.findViewById<TextView>(R.id.txtRejectError)

        val builder = AlertDialog.Builder(this)

        builder.setView(alertView)
        builder.setMessage("Reject Pengajuan")
        builder.setPositiveButton(
            "OK"
        ) { _, _ ->
            // Do Nothing
        }.setNegativeButton("Cancel") { dialog, _ ->
            dialog.dismiss()
        }

        val dialog = builder.create()
        dialog.show()
        dialog.getButton(AlertDialog.BUTTON_POSITIVE)
        .setOnClickListener {
            if (txtReject.text.toString().isBlank()) {
                txtReason.visibility = View.VISIBLE
            } else {
                viewModel.submitData(idPenggunaan, otr.toString(), txtReject.text.toString(), "Rejected" )
                containerLoadingDetailKomite.visibility = View.VISIBLE
                dialog.dismiss()
            }
        }


//        val alert =  MaterialAlertDialogBuilder(this)
//            .setTitle("Reject Pengajuan")
//            .setMessage("Masukkan alasan reject pengajuan")
//            .setView(alertView)
//           .set
//            .setNegativeButton("Cancel") { dialog, _ ->
//                dialog.dismiss()
//          }.setCancelable(false)
//            .create()
//
//
//        alert.setButton(AlertDialog.BUTTON_POSITIVE, "OK", object: DialogInterface.OnClickListener{
//            override fun onClick(dialog: DialogInterface?, which: Int) {
//                if (txtReject.text.toString().isBlank()) {
//                    txtReason.visibility = View.VISIBLE
//                } else {
//                    viewModel.submitData(idPenggunaan, otr.toString(), txtReject.text.toString(), "Rejected" )
//                    containerLoadingDetailKomite.visibility = View.VISIBLE
//                }
//            }
//        })
//        alert.show()
    }

    private fun alertAwc() {
        val alertView = layoutInflater.inflate(R.layout.alert_awc, null)

        val txtAwcReason = alertView.findViewById<EditText>(R.id.txtAwcReason)
        val txtAwcNominal = alertView.findViewById<EditText>(R.id.txtAwcNominal)
        val txtAwcEstimasi = alertView.findViewById<EditText>(R.id.txtAwcEstimasi)
        txtAwcNominal.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {}
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                var moneyValue = s.toString().replace(".","")
                if (moneyValue.isBlank()) moneyValue = "0"

                val decimalValue = moneyValue.toBigDecimal()

                val maxDisburse = decimalValue * BigDecimal(0.6)

                val result = SHFUtil.rupiahFormat(decimalValue)
                currentOtr = result
                if (currentOtr != s.toString()) {
                    txtAwcNominal.setText(currentOtr)
                    txtAwcNominal.setSelection(currentOtr.length)

                    txtAwcEstimasi.setText(SHFUtil.rupiahFormat(maxDisburse.setScale(0, RoundingMode.HALF_DOWN)))
                }
            }
        })

        val builder = AlertDialog.Builder(this)

        builder.setView(alertView)
        builder.setTitle("AWC Pengajuan")
        builder.setMessage("Masukkan alasan AWC dan OTR rekomendasi")
        builder.setPositiveButton(
            "OK"
        ) { _, _ ->
            // Do Nothing
        }.setNegativeButton("Cancel") { dialog, _ ->
            dialog.dismiss()
        }

        val dialog = builder.create()
        dialog.show()
        dialog.getButton(AlertDialog.BUTTON_POSITIVE)
            .setOnClickListener {
                val otr = txtAwcNominal.text.toString().replace(".","")

                if (txtAwcReason.text.toString().isBlank() ) {
                    alertView.findViewById<TextView>(R.id.txtAwcError).visibility = View.VISIBLE
                    alertView.findViewById<TextView>(R.id.txtAwcError).text = "Alasan AWC harus diisi !"
                } else if (otr.isBlank() || otr == "0" || otr.toBigDecimal() <= BigDecimal.ZERO ) {
                    alertView.findViewById<TextView>(R.id.txtAwcError).visibility = View.VISIBLE
                    alertView.findViewById<TextView>(R.id.txtAwcError).text = "Rekomendasi OTR harus diisi !"
                } else {
                    viewModel.submitData(idPenggunaan, txtAwcNominal.text.toString().replace(".", ""),
                        txtAwcReason.text.toString(), "Awc")
                    dialog.dismiss()
                    containerLoadingDetailKomite.visibility = View.VISIBLE
                }
            }

//        val alert = MaterialAlertDialogBuilder(this)
//            .setTitle("AWC Pengajuan")
//            .setView(alertView)
//            .setMessage("Masukkan alasan AWC dan OTR rekomendasi")
//            .setPositiveButton("OK") { dialog, _ ->
//                if (txtAwcReason.text.toString().isBlank()) {
//                    alertView.findViewById<TextView>(R.id.txtAwcError).visibility = View.VISIBLE
//                } else {
//                    viewModel.submitData(idPenggunaan, txtAwcNominal.text.toString().replace(".", ""),
//                        txtAwcReason.text.toString(), "Awc")
//                    dialog.dismiss()
//                    containerLoadingDetailKomite.visibility = View.VISIBLE
//                }
//            }.setNegativeButton("Cancel") { dialog, _ ->
//                dialog.dismiss()
//            }

    }

    private fun alertResponse() {
        val alertView = layoutInflater.inflate(R.layout.alert_response, null)

        if (!viewModel.isApproved) {
            alertView.findViewById<TextView>(R.id.responseHeader).text  = "Rejected"
            alertView.findViewById<ImageView>(R.id.responseImage).setImageResource(R.drawable.ic_large_delete)
        }

        MaterialAlertDialogBuilder(this)
            .setTitle("Pengajuan")
            .setView(alertView)
            .setPositiveButton("OK") { dialog, _ ->
                dialog.dismiss()
                setResult(Activity.RESULT_OK)
                finish()
            }.setCancelable(false).show()
    }

    private fun handleTracking(listTracking: List<StatusRekomendasi>?) {
        val result = ArrayList<StatusRekomendasi>()
        listTracking?.let { list ->
            list.forEach {
                if (it.status != null) {
                    result.add(it)
                }
            }
        }
        if (result.isNotEmpty()) {
            result.last().lastIndex = true
            if (adapter == null) {
                adapter = TrackingAdapter(result, this)

                detailTrackingRv.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
                detailTrackingRv.adapter = adapter
            }

        } else {
            containerTrackingPengajuan.visibility = View.GONE
        }
    }

}