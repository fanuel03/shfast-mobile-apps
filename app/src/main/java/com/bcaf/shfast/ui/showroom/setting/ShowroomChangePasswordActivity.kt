package com.bcaf.shfast.ui.showroom.setting

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.bcaf.shfast.R
import com.bcaf.shfast.persistence.api.Status
import com.bcaf.shfast.ui.LoginActivity
import com.bcaf.shfast.util.SessionManager
import com.bcaf.shfast.viewmodel.showroom.ShowroomChangePasswordViewModel
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import kotlinx.android.synthetic.main.change_password.*
import org.koin.android.viewmodel.ext.android.viewModel

class ShowroomChangePasswordActivity: AppCompatActivity() {
    private val viewModel: ShowroomChangePasswordViewModel by viewModel()

    companion object {
        @JvmStatic
        fun getIntent(context: Context): Intent {
            return Intent(context, ShowroomChangePasswordActivity::class.java)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.change_password)
        supportActionBar!!.title = "Ganti Password"
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)

        initEvent()
        observe()
    }

    private fun initEvent() {
        btn_change_password.setOnClickListener {
            val msg = checkValidity()
            if (msg == null) {
                val oldPassword = text_change_password.text.toString()
                val newPassword = text_change_new.text.toString()
                viewModel.changePassword(oldPassword, newPassword)
            } else {
                showErrorDialog(msg)
            }
        }

        btn_back_change_password.setOnClickListener {
            finish()
        }
    }

    private fun observe() {
        viewModel.passwordData.observe(this, Observer {
            if (it.status == Status.SUCCESS) {
                showSuccess()
            } else {
                showErrorDialog(it.message ?: "Terjadi kesalahan ketika mengganti password")
            }
        })
    }

    private fun checkValidity(): String? {
        if (text_change_password.text.toString().isNullOrBlank()) {
            return "Password harus diisi"
        } else if (text_change_new.text.toString().isNullOrBlank()) {
            return "Password baru harus diisi"
        } else if (text_change_confirm.text.toString().isNullOrBlank()) {
            return "Konfirmasi password baru harus diisi"
        } else if (text_change_confirm.text.toString() != text_change_new.text.toString()) {
            return "Password baru tidak sama dengan konfirmasi password"
        }

        return null
    }

    private fun showErrorDialog(msg: String) {
        MaterialAlertDialogBuilder(this)
            .setTitle("Terjadi Kesalahan")
            .setMessage(msg)
            .setPositiveButton("OK") { dialog, _ ->
                dialog.dismiss()
            }.setCancelable(false).show()
    }

    private fun showSuccess() {
        MaterialAlertDialogBuilder(this)
            .setTitle("Konfirmasi")
            .setMessage("Password berhasil diganti")
            .setPositiveButton("OK") { dialog, _ ->
                dialog.dismiss()
                SessionManager.flush(this)
                startActivity(LoginActivity.getIntent(this).apply {
                    flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                })
            }.setCancelable(false).show()
    }
}