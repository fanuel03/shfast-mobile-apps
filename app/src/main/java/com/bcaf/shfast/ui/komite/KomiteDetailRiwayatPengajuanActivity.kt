package com.bcaf.shfast.ui.komite

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.bcaf.shfast.BuildConfig
import com.bcaf.shfast.R
import com.bcaf.shfast.persistence.api.Status
import com.bcaf.shfast.persistence.response.komite.DetailPengajuanKomite
import com.bcaf.shfast.persistence.response.komite.StatusRekomendasi
import com.bcaf.shfast.recyclerview.adapter.TrackingAdapter
import com.bcaf.shfast.ui.WebViewActivity
import com.bcaf.shfast.util.SHFUtil
import com.bcaf.shfast.viewmodel.komite.KomiteDetailRiwayatApprovalViewModel
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_detail_history_komite.*
import org.koin.android.viewmodel.ext.android.viewModel
import java.math.BigDecimal
import java.math.RoundingMode

class KomiteDetailRiwayatPengajuanActivity: AppCompatActivity() {
    private val viewModel: KomiteDetailRiwayatApprovalViewModel by viewModel()

    private lateinit var idPenggunaan: String
    private var adapter: TrackingAdapter? = null

    companion object {
        private const val EXTRA_ID_PENGAJUAN = "EXTRA_ID_PENGAJUAN"
        private const val BASE_URL = BuildConfig.BASE_URL
        @JvmStatic
        fun getIntent(context: Context, idPenggunaan: String): Intent {
            return Intent(context, KomiteDetailRiwayatPengajuanActivity::class.java).apply {
                putExtra(EXTRA_ID_PENGAJUAN, idPenggunaan)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_history_komite)
        supportActionBar!!.hide()

        idPenggunaan = intent.getStringExtra(EXTRA_ID_PENGAJUAN)!!

        initEvent()
        observe()

        containerLoadingHistoryKomite.visibility = View.VISIBLE
        viewModel.loadData(idPenggunaan)
    }

    private fun initEvent() {
        Glide.with(this).load(R.drawable.loading).into(imgLoading)
        btnBackHistoryKomite.setOnClickListener {
            finish()
        }
    }

    private fun observe() {
        viewModel.detailData.observe(this, Observer {
            containerLoadingHistoryKomite.visibility = View.GONE

            if (it.status == Status.SUCCESS) {
                if (it.data is DetailPengajuanKomite) {
                    val response = it.data
                    txtHistoryNamaShowroom.text = response.namaShowroom ?: "-"
                    txtHistoryWilayahShowroom.text = response.wilayahShowroom ?: "-"
                    txtHistoryLoanOtr.text = "Rp. "+SHFUtil.rupiahFormat(response.jumlahPenggunaan?.toBigDecimal()?.setScale(0, RoundingMode.HALF_EVEN)!!)
                    txtHistoryLoadPh.text = "Rp." +SHFUtil.rupiahFormat((response.jumlahPenggunaan?.toBigDecimal()!!.multiply(BigDecimal(0.6))).setScale(0,RoundingMode.HALF_EVEN))
                    txtHistoryVehicleTipe.text = response.type ?: "-"
                    txtHistoryVehicleBrand.text = response.merk ?: "-"
                    txtHistoryVehicleLicense.text = response.noPol ?: "-"
                    txtHistoryVehicleReqDate.text = SHFUtil.parseTime(response.createdAt!!)
                    txtHistoryVehicleYear.text = response.year ?: "-"

                    handleTracking(response.statusRekomendasi)

                    btnHistoryDokumenBpkb.setOnClickListener {
                        startActivity(
                            WebViewActivity.getIntent(this, "BPKB",
                                BASE_URL + response.fotoBPKB))
                    }

                    btnHistoryDokumenStnk.setOnClickListener {
                        startActivity(WebViewActivity.getIntent(this, "STNK", BASE_URL + response.fotoSTNK))
                    }

                    btnHistoryDokumenFaktur.setOnClickListener {
                        startActivity(
                            WebViewActivity.getIntent(this, "Faktur",
                                BASE_URL + response.fotoFaktur))
                    }

                    btnHistoryDokumenFoken.setOnClickListener {
                        startActivity(
                            WebViewActivity.getIntent(this, "Foto Kendaraan",
                                BASE_URL + response.fotoTampakDepan))
                    }

                    btnHistoryOlx.setOnClickListener {
                        startActivity(WebViewActivity.getIntent(this, "OLX", SHFUtil.buildUrlOLX(response.merk ?: "", response.type ?: "")))
                    }

                    btnHistoryCarmudi.setOnClickListener {
                        startActivity(
                            WebViewActivity.getIntent(this, "Carmudi",
                                SHFUtil.buildUrlCarmudi(response.merk ?: "", response.type ?: "")))
                    }


                } else {
                    Toast.makeText(this, "Terjadi kesalahan ketika mengambil data", Toast.LENGTH_LONG).show()
                }
            } else {
                Toast.makeText(this, "Terjadi kesalahan ketika mengambil data", Toast.LENGTH_LONG).show()
            }
        })
    }

    private fun handleTracking(listTracking: List<StatusRekomendasi>?) {
        val result = ArrayList<StatusRekomendasi>()
        listTracking?.let { list ->
            list.forEach {
                if (it.status != null) {
                    result.add(it)
                }
            }
        }
        if (result.isNotEmpty()) {
            result.last().lastIndex = true
            if (adapter == null) {
                adapter = TrackingAdapter(result, this)

                detailHistoryRv.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
                detailHistoryRv.adapter = adapter
            }

        } else {
            containerTracking.visibility = View.GONE
        }
    }
}