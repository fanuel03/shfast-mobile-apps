package com.bcaf.shfast.ui.komite

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.bcaf.shfast.R
import com.bcaf.shfast.persistence.api.Status
import com.bcaf.shfast.persistence.model.komite.PengajuanKomite
import com.bcaf.shfast.persistence.response.dashboard.BucketKomiteBody
import com.bcaf.shfast.recyclerview.adapter.PengajuanKomiteAdapter
import com.bcaf.shfast.recyclerview.callback.ApprovalCallback
import com.bcaf.shfast.viewmodel.komite.KomiteRiwayatApprovalViewModel
import com.bumptech.glide.Glide
import com.google.android.material.chip.Chip
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import kotlinx.android.synthetic.main.activity_history_pengajuan_komite.*
import org.koin.android.viewmodel.ext.android.viewModel
import java.util.*
import kotlin.collections.ArrayList
import kotlin.concurrent.schedule

class KomiteRiwayatPengajuanActivity: AppCompatActivity() {
    private var adapter: PengajuanKomiteAdapter? = null
    private val viewModel: KomiteRiwayatApprovalViewModel by viewModel()

    private val list: ArrayList<PengajuanKomite> = ArrayList()

    private val sortOptions = arrayOf("Tanggal Terbaru", "Tanggal Terlama")
    private var checkedItem = 0
    private var choosenItem = 0

    companion object {
        @JvmStatic
        fun getIntent(context: Context): Intent {
            return Intent(context, KomiteRiwayatPengajuanActivity::class.java)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_history_pengajuan_komite)
        Glide.with(this).load(R.drawable.loading).into(imgLoading)
        supportActionBar!!.hide()

        initEvent()
        initRv()
        observe()

        refresh()
    }

    private fun refresh() {
        list.clear()
        containerRvHistoryKomite.visibility = View.GONE
        containerErrorHistoryKomite.visibility = View.GONE

        loadingHistoryKomite.visibility = View.VISIBLE

        Timer("Setup", false).schedule(1000) {
            viewModel.refreshData()
        }
    }

    private fun initEvent() {

        chip_group.setOnCheckedChangeListener { _, checkedId ->
            val chip = chip_group.findViewById<Chip>(checkedId)
            chip?.let {
                viewModel.setStatus(it.text.toString())
                refresh()
            }
        }

        filter.setOnClickListener {
            MaterialAlertDialogBuilder(this)
                .setTitle("Urutkan berdasarkan:")
                .setNeutralButton(resources.getString(R.string.cancel)) { dialog, _ ->
                    dialog.dismiss()
                }
                .setPositiveButton(resources.getString(R.string.ok)) { dialog, _ ->
                    checkedItem = choosenItem
                    viewModel.setSort(checkedItem)
                    refresh()
                    dialog.dismiss()
                }
                // Single-choice items (initialized with checked item)
                .setSingleChoiceItems(sortOptions, checkedItem) { _, which ->
                    choosenItem = which
                }
                .show()
        }

        btnRefreshKomite.setOnClickListener {
            refresh()
        }
    }

    private fun initRv() {
        if (adapter == null) {
            adapter = PengajuanKomiteAdapter(list, this, object: ApprovalCallback {
                override fun onClick(pengajuan: PengajuanKomite) {
                    startActivity(KomiteDetailRiwayatPengajuanActivity.getIntent(this@KomiteRiwayatPengajuanActivity, pengajuan.idPenggunaan))
                }
            })

            rvHistoryKomite.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
            rvHistoryKomite.adapter = adapter
        }
    }

    private fun observe() {
        viewModel.historyData.observe(this, androidx.lifecycle.Observer {
            loadingHistoryKomite.visibility = View.GONE
            if(it.status == Status.SUCCESS) {
                if (it.data is BucketKomiteBody) {
                    val response = it.data
                    if(response.listPenggunaan.isNullOrEmpty()) {
                        showErrorView(true)
                    } else {
                        response.listPenggunaan!!.forEach { penggunaan->
                            list.add(penggunaan.toPengajuanKomite())
                        }
                        containerRvHistoryKomite.visibility = View.VISIBLE
                        adapter?.notifyDataSetChanged()
                    }
                } else {
                    showErrorView()
                }
            } else {
                showErrorView()
            }
        })
    }

    private fun showErrorView(isDataEmpty: Boolean = false) {
        containerRvHistoryKomite.visibility = View.GONE
        containerErrorHistoryKomite.visibility = View.VISIBLE
        if (isDataEmpty) {
            errorHeaderKomite.text = "Belum ada pengajuan"
            errorImageKomite.setImageResource(R.drawable.empty)
        } else {
            errorHeaderKomite.text = "Terjadi kesalahan"
            errorImageKomite.setImageResource(R.drawable.sad)
        }
    }
}