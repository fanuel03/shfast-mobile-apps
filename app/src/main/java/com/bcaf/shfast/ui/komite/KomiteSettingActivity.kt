package com.bcaf.shfast.ui.komite

import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.bcaf.shfast.R
import com.bcaf.shfast.persistence.api.Status
import com.bcaf.shfast.ui.LoginActivity
import com.bcaf.shfast.util.SessionManager
import com.bcaf.shfast.viewmodel.komite.KomiteSettingViewModel
import com.bumptech.glide.Glide
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.firebase.crashlytics.FirebaseCrashlytics
import kotlinx.android.synthetic.main.activity_setting.btnLogout
import kotlinx.android.synthetic.main.activity_setting.imgLoading
import kotlinx.android.synthetic.main.activity_setting.loading_setting
import kotlinx.android.synthetic.main.activity_setting_komite.*
import org.koin.android.viewmodel.ext.android.viewModel


class KomiteSettingActivity: AppCompatActivity() {

    private val viewModel: KomiteSettingViewModel by viewModel()

    companion object {
        @JvmStatic
        fun getIntent(context: Context): Intent {
            return Intent(context, KomiteSettingActivity::class.java)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_setting_komite)
        supportActionBar!!.title = "Pengaturan"
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)

        initEvent()
        observe()
    }

    private fun initEvent() {
        Glide.with(this).load(R.drawable.loading).into(imgLoading)

        btnLogout.setOnClickListener {
            MaterialAlertDialogBuilder(this)
                .setTitle("Konfirmasi")
                .setMessage("Apakah anda ingin logout ?")
                .setPositiveButton("Ya") { _, _ ->
                    //loading_setting.visibility = View.VISIBLE
                    loading_setting.visibility = View.VISIBLE
                    viewModel.logout()
                }
                .setNegativeButton("Tidak") { dialog, _ ->
                    dialog.dismiss()
                }
                .show()
        }

        basic_contact_us_komite.setOnClickListener {
            val pm = packageManager
            try {
                val text = "Halo, ada kendala di SHFast: \n"
                val url = Uri.parse(
                    String.format("https://api.whatsapp.com/send?phone=%s&text=%s", "6281322668285", text)
                )

                val waIntent = Intent(Intent.ACTION_VIEW, url)

                val info =
                    pm.getPackageInfo("com.whatsapp", PackageManager.GET_META_DATA)
                //Check if package exists or not. If not then code
                //in catch block will be called
                waIntent.setPackage("com.whatsapp")
                waIntent.putExtra(Intent.EXTRA_TEXT, text)
                startActivity(Intent.createChooser(waIntent, "Share with"))
            } catch (e: PackageManager.NameNotFoundException) {
                Toast.makeText(this, "WhatsApp not Installed", Toast.LENGTH_SHORT)
                    .show()
            }

        }
    }

    private fun observe() {
        viewModel.logoutData.observe(this, Observer {
            loading_setting.visibility = View.GONE
            if (it.status == Status.SUCCESS) {
                SessionManager.flush(this)
                startActivity(LoginActivity.getIntent(this).apply {
                    flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                })
            } else {
                FirebaseCrashlytics.getInstance().log("Error Logout: ${it.message.toString()}")
                Toast.makeText(this, "Terjadi kesalahan ketika mengirim data", Toast.LENGTH_LONG).show()
            }
        })
    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                finish()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
}