package com.bcaf.shfast.ui.komite

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.bcaf.shfast.R
import com.bcaf.shfast.persistence.api.Status
import com.bcaf.shfast.persistence.model.komite.PengajuanKomite
import com.bcaf.shfast.persistence.response.dashboard.BucketKomiteBody
import com.bcaf.shfast.recyclerview.adapter.PengajuanKomiteAdapter
import com.bcaf.shfast.recyclerview.callback.ApprovalCallback
import com.bcaf.shfast.viewmodel.komite.KomiteBucketApprovalViewModel
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_pengajuan_komite.*
import org.koin.android.viewmodel.ext.android.viewModel
import java.util.regex.Pattern

class KomitePengajuanActivity: AppCompatActivity() {

    private val viewModel: KomiteBucketApprovalViewModel by viewModel()

    private var sourceList: ArrayList<PengajuanKomite> = arrayListOf()
    private var shownList: ArrayList<PengajuanKomite> = arrayListOf()

    private var adapter: PengajuanKomiteAdapter? = null
    private var stateSearch = false
    private var searchParam = ""

    companion object {
        @JvmStatic
        fun getIntent(context: Context): Intent {
            return Intent(context, KomitePengajuanActivity::class.java)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pengajuan_komite)
        supportActionBar!!.hide()

        initEvent()
        initRv()
        observe()

        refresh()
    }

    private fun refresh() {
        sourceList.clear()
        shownList.clear()
        adapter?.notifyDataSetChanged()
        containerLoadingApprovalKomite.visibility = View.VISIBLE
        viewModel.refreshData()
    }

    private fun initEvent() {
        Glide.with(this).load(R.drawable.loading).into(imgLoading)

        actionApprovalKomite.setOnClickListener {
            stateSearch = !stateSearch
            if (stateSearch) {
                headerAppbarApprovalKomite.visibility = View.GONE
                txtSearchApprovalKomite.visibility = View.VISIBLE
                txtSearchApprovalKomite.setText("")
                txtSearchApprovalKomite.requestFocus()
                actionApprovalKomite.setImageResource(R.drawable.ic_close)

            } else {
                headerAppbarApprovalKomite.visibility = View.VISIBLE
                txtSearchApprovalKomite.visibility = View.GONE
                actionApprovalKomite.setImageResource(R.drawable.ic_search)
            }
        }

        btnBackApprovalKomite.setOnClickListener {
            finish()
        }

        btnRefreshApprovaLKomite.setOnClickListener {
            containerLoadingApprovalKomite.visibility = View.VISIBLE
            errorContainerApprovalKomite.visibility = View.GONE
            if (stateSearch) actionApprovalKomite.performClick()
            actionApprovalKomite.visibility = View.GONE
            containerRvApprovalKomite.visibility = View.GONE

            refresh()
        }

        txtSearchApprovalKomite.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {}
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                shownList.clear()
                if (s.isNotBlank()){
                   shownList.addAll(searchPengajuan(s.toString()))
                   adapter?.notifyDataSetChanged()
               } else {
                   shownList.addAll(sourceList)
                   adapter?.notifyDataSetChanged()
               }
            }
        })

    }

    private fun searchPengajuan(key: String): ArrayList<PengajuanKomite> {
        val result = arrayListOf<PengajuanKomite>()
        val pattern = Pattern.compile(key, Pattern.CASE_INSENSITIVE)
        sourceList.forEach {
             if (it.noPlat.contains(pattern.toRegex()) || it.namaShowroom.contains(pattern.toRegex())) {
                result.add(it)
            }
        }
        return result
    }

    private fun initRv() {
        if (adapter == null) {
            adapter = PengajuanKomiteAdapter(shownList, this, object: ApprovalCallback {
                override fun onClick(pengajuan: PengajuanKomite) {
                    startActivityForResult(
                        KomiteDetailPengajuanActivity.getIntent(this@KomitePengajuanActivity, pengajuan.idPenggunaan),
                        KomiteDetailPengajuanActivity.REQUEST_APPROVAL
                    )
                }
            })

            rv_bucket_pengajuan.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
            rv_bucket_pengajuan.adapter = adapter
        }
    }

    private fun observe(){
        viewModel.bucketData.observe(this, Observer {
            if (it.status == Status.SUCCESS) {
                if (it.data is BucketKomiteBody) {
                    val response = it.data

                    if(response.listPenggunaan.isNullOrEmpty()) {
                        showError(true)
                    } else {
                        sourceList.clear()
                        shownList.clear()
                        response.listPenggunaan!!.forEach { penggunaan->
                            sourceList.add(penggunaan.toPengajuanKomite())
                        }
                        actionApprovalKomite.visibility = View.VISIBLE
                        shownList.addAll(sourceList)
                        containerLoadingApprovalKomite.visibility = View.GONE
                        containerRvApprovalKomite.visibility = View.VISIBLE
                        adapter?.notifyDataSetChanged()
                    }

                } else {
                    showError()
                }
            } else {
                showError()
            }
        })
    }

    private fun showError(emptyData: Boolean = false) {
        containerLoadingApprovalKomite.visibility = View.GONE
        errorContainerApprovalKomite.visibility = View.VISIBLE
        if (emptyData) {
            error_image.setImageResource(R.drawable.empty)
            error_header.text = "Belum ada pengajuan"
        } else {
            error_image.setImageResource(R.drawable.sad)
            error_header.text = "Terjadi kesalahan"
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == KomiteDetailPengajuanActivity.REQUEST_APPROVAL) {
            refresh()
        }
    }



}