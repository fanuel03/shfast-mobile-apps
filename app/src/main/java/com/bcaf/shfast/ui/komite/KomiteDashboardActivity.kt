package com.bcaf.shfast.ui.komite

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.bcaf.shfast.R
import com.bcaf.shfast.util.SessionManager
import kotlinx.android.synthetic.main.activity_dashboard_komite.*

class KomiteDashboardActivity: AppCompatActivity() {

    private var doubleBackToExitPressedOnce = false

    companion object {
        @JvmStatic
        fun getIntent(context: Context): Intent {
            return Intent(context, KomiteDashboardActivity::class.java)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashboard_komite)
        supportActionBar?.hide()
        initProfile()
        initEvent()
    }

    private fun initProfile() {
        txtKomiteName.text = SessionManager.getNama(this)
        var role = SessionManager.getRole(this)!!.replace("ROLE_","")
        when (role) {
            "KOMITE2" -> role = "DIV. HEAD LEASING & SPECIAL FINANCING"
            "RM" -> role = "Regional Manager"
            "BM" -> role = "Branch Manager"
        }
        txtKomiteRole.text = role
    }

    private fun initEvent() {
        btnPengajuan.setOnClickListener {
            startActivity(KomitePengajuanActivity.getIntent(this))
        }

        btnRiwayat.setOnClickListener {
            startActivity(KomiteRiwayatPengajuanActivity.getIntent(this))
        }

        btnPengaturan.setOnClickListener {
            startActivity(KomiteSettingActivity.getIntent(this))
        }
    }

    override fun onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed()
            return
        }

        this.doubleBackToExitPressedOnce = true
        Toast.makeText(this, "Tekan Kembali sekali lagi untuk keluar", Toast.LENGTH_SHORT).show()

        Handler().postDelayed(kotlinx.coroutines.Runnable { doubleBackToExitPressedOnce = false }, 2000)
    }

}