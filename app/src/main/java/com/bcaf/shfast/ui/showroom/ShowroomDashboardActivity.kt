package com.bcaf.shfast.ui.showroom

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.widget.Toast
import androidx.core.content.res.ResourcesCompat
import androidx.lifecycle.Observer
import com.bcaf.shfast.R
import com.bcaf.shfast.ext.SHFFragment
import com.bcaf.shfast.ext.ScopedAppActivity
import com.bcaf.shfast.persistence.api.Status
import com.bcaf.shfast.ui.showroom.fragment.ShowroomDashboardFragment
import com.bcaf.shfast.ui.showroom.fragment.ShowroomHistoryFragment
import com.bcaf.shfast.ui.showroom.fragment.ShowroomPengajuanFragment
import com.bcaf.shfast.util.SessionManager
import com.bcaf.shfast.viewmodel.showroom.ShowroomListener
import com.google.android.material.bottomappbar.BottomAppBar
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import kotlinx.android.synthetic.main.activity_showroom.*
import kotlinx.coroutines.Runnable
import kotlinx.coroutines.launch
import org.koin.android.ext.android.inject
import java.text.SimpleDateFormat
import java.util.*

class ShowroomDashboardActivity: ScopedAppActivity() {

    private val listener: ShowroomListener by inject()
    private lateinit var fragment: SHFFragment
    private var doubleBackToExitPressedOnce = false

    companion object {
        @JvmStatic
        fun getIntent(
            context: Context
        ): Intent {
            return Intent(context, ShowroomDashboardActivity::class.java)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_showroom)
        supportActionBar?.hide()

        initEvent()
        observe()
    }

    private fun exceedPeriod(): Boolean {
        val calendar = Calendar.getInstance()
        calendar.add(Calendar.MONTH, 1)

        val sdf = SimpleDateFormat("yyyy-MM-dd", Locale.US)
        val now = sdf.parse(SessionManager.getTanggalExpired(this) ?: "2020-01-01")
        if (calendar.time.after(now) ) {
            return true
        }
        return false
    }

    private fun initEvent() {
        swipe_refresh.isEnabled = false

        bar.setOnMenuItemClickListener { menuItem ->
            setFabCenter()
            when (menuItem.itemId) {
                R.id.riwayat -> {
                    fragment = ShowroomHistoryFragment()
                    switchFragment()

                    true
                }
//                R.id.pelunasan -> {
//                    fragment = ShowroomContractFragment()
//                    switchFragment()
//                    true
//                }
                R.id.pengaturan -> {
                    startActivity(ShowroomSettingActivity.getIntent(this))
                    true
                }
                else -> false
            }
        }

        fab.setOnClickListener {
            if (exceedPeriod()) {
                MaterialAlertDialogBuilder(this)
                    .setTitle("Plafon Expired")
                    .setMessage("Masa berlaku plafond Anda telah habis, silahkan ajukan plafond kembali")
                    .setPositiveButton("OK") { dialog, _ ->
                        dialog.dismiss()
                    }.show()
            } else {
                startTask()
                if (fragment is ShowroomPengajuanFragment) {
//                 swipe_refresh.isEnabled = false
                    fragment = ShowroomDashboardFragment()

                } else {
//                 swipe_refresh.isEnabled =false
                    fragment = ShowroomPengajuanFragment()
                }
                switchFragment()
            }
        }

        bar.setNavigationOnClickListener {
            setFabCenter()

            fragment = ShowroomDashboardFragment()
            switchFragment()
        }

        fragment = ShowroomDashboardFragment()
        switchFragment()
    }

    private fun startTask() = launch {
        if (fragment is ShowroomDashboardFragment) {
            bar.fabAlignmentMode = BottomAppBar.FAB_ALIGNMENT_MODE_CENTER
            fab.setImageDrawable(ResourcesCompat.getDrawable(resources, R.drawable.ic_baseline_add_24, theme))

        } else {
            bar.fabAlignmentMode = BottomAppBar.FAB_ALIGNMENT_MODE_END
            fab.setImageDrawable(ResourcesCompat.getDrawable(resources, R.drawable.ic_back_fab_white, theme))
        }
    }

    private fun setFabCenter() = launch {
        bar.fabAlignmentMode = BottomAppBar.FAB_ALIGNMENT_MODE_CENTER
        fab.setImageDrawable(ResourcesCompat.getDrawable(resources, R.drawable.ic_baseline_add_24, theme))
    }

    private fun switchFragment() {
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.fragment_container, fragment)
            .commit()
    }

    private fun observe() {
        listener.isSubmittingPengajuan.observe(this, Observer { submitStatus ->
            if (submitStatus.status == Status.LOADING) {
                bar.isEnabled = false
                fab.isEnabled = false
            } else {
                bar.isEnabled = true
                fab.isEnabled = true

                if (submitStatus.status == Status.SUCCESS) {
                    fab.performClick()
                }
            }

        })
    }

    override fun onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed()
            return
        }

        this.doubleBackToExitPressedOnce = true
        Toast.makeText(this, "Tekan Kembali sekali lagi untuk keluar", Toast.LENGTH_SHORT).show()

        Handler().postDelayed(Runnable { doubleBackToExitPressedOnce = false }, 2000)
    }
}