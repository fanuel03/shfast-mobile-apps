package com.bcaf.shfast.ui.showroom.fragment

import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.annotation.StringRes
import androidx.core.content.FileProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.bcaf.shfast.R
import com.bcaf.shfast.ext.SHFFragment
import com.bcaf.shfast.persistence.api.Status
import com.bcaf.shfast.persistence.model.Dokumen
import com.bcaf.shfast.persistence.response.showroom.RekeningShowroom
import com.bcaf.shfast.recyclerview.adapter.UploadAdapter
import com.bcaf.shfast.recyclerview.callback.UploadCallback
import com.bcaf.shfast.util.SHFUtil
import com.bcaf.shfast.util.SessionManager
import com.bcaf.shfast.viewmodel.showroom.ShowroomListener
import com.bcaf.shfast.viewmodel.showroom.ShowroomPengajuanViewModel
import com.bumptech.glide.Glide
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import kotlinx.android.synthetic.main.fragment_pengajuan_showroom.*
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.viewModel
import permissions.dispatcher.*
import java.io.File
import java.io.IOException
import java.math.BigDecimal
import java.math.RoundingMode
import java.text.SimpleDateFormat
import java.util.*

@RuntimePermissions
class ShowroomPengajuanFragment: SHFFragment() {

    companion object {
        const val MAX_OTR = "MAX_OTR"
    }

    private val viewModel: ShowroomPengajuanViewModel by viewModel()
    private val listener: ShowroomListener by inject()

    private var adapter: UploadAdapter? = null
    private val list: ArrayList<Dokumen> = SHFUtil.getDokumenPersyaratan()
    private val REQUEST_TAKE_PHOTO = 1

    private var currentPath = ""
    private var currentPos = 0
    private var currentOtr = "0"
    private var selectedNoAkun = ""
    private var maxOtr = BigDecimal.ZERO
    val items = arrayListOf<String>()
    private var listRekening = arrayListOf<RekeningShowroom>()


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        maxOtr = listener.plafond.value!! / BigDecimal(0.6)

        return inflater.inflate(R.layout.fragment_pengajuan_showroom, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        initEvent()
        initRv()
        observe()
    }

    override fun refreshData() {}


    private fun getTenorEstimation(): Int {
        val calendar1 = Calendar.getInstance()
        //calendar.add(Calendar.MONTH, 1)

        val sdf = SimpleDateFormat("yyyy-MM-dd", Locale.US)
        val exp = sdf.parse(SessionManager.getTanggalExpired(context!!) ?: "2020-01-01")
        val calendar2 = Calendar.getInstance()
        calendar2.time = exp!!

        val m1: Int = (calendar1.get(Calendar.YEAR) * 12 * 30) + calendar1.get(Calendar.MONTH) * 30 + calendar1.get(Calendar.DAY_OF_MONTH)
        val m2: Int = (calendar2.get(Calendar.YEAR) * 12 * 30) + calendar2.get(Calendar.MONTH) * 30 + calendar2.get(Calendar.DAY_OF_MONTH)
        return m2 - m1 + 1
    }

    private fun initEvent() {
        Glide.with(this).load(R.drawable.loading).into(imgLoading)

        listRekening = SessionManager.getNorek(activity!!)
        listRekening.forEach {
            it.accNo?.let { accNo -> items.add(accNo)  }
        }

        val adapter = ArrayAdapter(requireContext(), R.layout.item_no_rekening, items)
        autoCompleteNorek?.setAdapter(adapter)
        autoCompleteNorek.setOnItemClickListener { _, _, position, _ ->
            selectedNoAkun = listRekening[position].accNo ?: ""
            //Toast.makeText(activity!!, items[position], Toast.LENGTH_SHORT).show()
            txtAtasNama.text = listRekening[position].accAtasNama ?: "-"
            txtBankRekening.text = listRekening[position].accBankName ?: "-"
            txtCabang.text = listRekening[position].accBranch ?: "-"
        }
        autoCompleteNorek.keyListener = null

        if (items.size == 1) {
            autoCompleteNorek.setText(items[0], false)
            selectedNoAkun = listRekening[0].accNo ?: ""
            //Toast.makeText(activity!!, items[position], Toast.LENGTH_SHORT).show()
            txtAtasNama.text = listRekening[0].accAtasNama ?: "-"
            txtBankRekening.text = listRekening[0].accBankName ?: "-"
            txtCabang.text = listRekening[0].accBranch ?: "-"
        }

        val tenorEst = getTenorEstimation()
        if (tenorEst < 30) {
            txtEstimasiTenor.setTextColor(Color.RED)
            txtEstimasiTenor.setText("0 Bulan")
        } else {
            when (tenorEst) {
                in 30..59 -> {
                    txtEstimasiTenor.setText("1 Bulan")
                }
                in 60..89 -> {
                    txtEstimasiTenor.setText("2 Bulan")
                }
                else -> {
                    txtEstimasiTenor.setText("3 Bulan")
                }
            }

        }
        txtExpired.setText(SHFUtil.parseTime(SessionManager.getTanggalExpired(context!!) ?: "01 Januari 1990"))

        btnSubmit.setOnClickListener {
            if (validateInput()) {
                listener.isSubmitting()

                loading_pengajuan.visibility = View.VISIBLE
                viewModel.startPengajuan(list, txtHarga.text.toString().replace(".", "").toInt(), txtNopol.text.toString(), selectedNoAkun)
            } else {
                loading_pengajuan.visibility = View.GONE
            }
        }

        txtHarga.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {}
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                var moneyValue = s.toString().replace(".","")
                if (moneyValue.isBlank()) moneyValue = "0"

                var decimalValue = moneyValue.toBigDecimal()
                if (decimalValue > maxOtr) decimalValue = maxOtr

                val maxDisburse = decimalValue * BigDecimal(0.6)

                val result = SHFUtil.rupiahFormat(decimalValue)
                currentOtr = result
                if (currentOtr != s.toString()) {
                    txtHarga.setText(currentOtr)
                    txtHarga.setSelection(currentOtr.length)

                    txtEstimasiPencairan.setText(SHFUtil.rupiahFormat(maxDisburse.setScale(0, RoundingMode.HALF_DOWN)))
                }
            }
        })
    }

    private fun validateInput(): Boolean{
        val harga = txtHarga.text.toString()
        val nopol = txtNopol.text.toString()

        if (harga.isBlank() || harga.replace(".", "").toInt() <= 0) {
            alertError("Pengajuan Invalid", "OTR harus diisi")
            return false
        }

        if (nopol.isBlank()) {
            alertError("Pengajuan Invalid", "Nopol harus diisi")
            return false
        }

        list.forEach { dokumen ->
            if (dokumen.path == null || dokumen.path == "") {
                alertError("Pengajuan Invalid", "Dokumen yang dipilih belum lengkap (${dokumen.name})")
                return false

            }
        }

        return true
    }

    private fun observe() {
        viewModel.pengajuanData.observe(this, androidx.lifecycle.Observer {
            if (it.status == Status.SUCCESS) {
                MaterialAlertDialogBuilder(this.context!!)
                    .setTitle("Pengajuan Berhasil")
                    .setMessage("Tekan OK untuk kembali ke Halaman Utama")
                    .setPositiveButton("OK") { _, _ ->
                        listener.submittingDone(isSuccess = true)
                    }.setCancelable(false)
                    .show()
            } else if (it.status == Status.ERROR) {
                loading_pengajuan.visibility = View.GONE

                alertError(message = it.message.toString())
                listener.submittingDone(isSuccess = false)
            }

        })

        viewModel.uploadData.observe(this, androidx.lifecycle.Observer {
            txtProgress.text = it
        })
    }

    private fun alertError(title: String = "Terjadi Kesalahan", message: String) {
        MaterialAlertDialogBuilder(this.context!!)
            .setTitle(title)
            .setMessage(message)
            .setPositiveButton("OK") { dialog, _ ->
                dialog.dismiss()
            }
            .setCancelable(false)
            .show()
    }

    private fun initRv() {
        if (adapter == null) {
            adapter = UploadAdapter(list, activity!!.applicationContext, object:
                UploadCallback {
                override fun onAddImage(position: Int) {
                    //takePictureWith(position)
                    takePictureWithPermissionCheck(position)
                }
            })
            rv_upload.layoutManager = LinearLayoutManager(activity!!.applicationContext, LinearLayoutManager.VERTICAL, false)
            rv_upload.adapter = adapter
            adapter?.notifyDataSetChanged()
        }
    }

    @Throws(IOException::class)
    private fun createImageFile(position: Int): File {
        // Create an image file name
        val timeStamp: String = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val storageDir: File = this.context!!.getExternalFilesDir(Environment.DIRECTORY_PICTURES)!!
        return File.createTempFile(
            "JPEG_${timeStamp}_", /* prefix */
            ".jpg", /* suffix */
            storageDir /* directory */
        ).apply {
            // Save a file: path for use with ACTION_VIEW intents
            currentPath = absolutePath
        }
    }
    @NeedsPermission(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
    fun takePicture(position: Int) {
        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
            // Ensure that there's a camera activity to handle the intent
            takePictureIntent.resolveActivity(this.context!!.packageManager)?.also {
                // Create the File where the photo should go
                val photoFile: File? = try {
                    currentPos = position
                    createImageFile(position)
                } catch (ex: IOException) {
                    // Error occurred while creating the File

                    null
                }
                // Continue only if the File was successfully created
                photoFile?.also {
                    val photoURI: Uri = FileProvider.getUriForFile(
                        this.context!!,
                        "com.bcaf.shfast.fileprovider",
                        it
                    )
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                    startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO)
                }
            }
        }
    }

    @OnShowRationale(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
    fun showRationaleForCamera(request: PermissionRequest) {
         showRationaleDialog(R.string.camera_rationale, request)
    }

    @OnPermissionDenied(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
    fun onCameraDenied() {
        Toast.makeText(this.context, "Akses File dan Kamera dibutuhkan untuk pengajuan", Toast.LENGTH_SHORT).show()
    }

    @OnNeverAskAgain(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
    fun onCameraNeverAskAgain() {
        Toast.makeText(this.context, "Anda tidak dapat melakukan pengajuan jika menolak akses Kamera dan File", Toast.LENGTH_SHORT).show()
    }

    private fun showRationaleDialog(@StringRes messageResId: Int, request: PermissionRequest) {
        AlertDialog.Builder(context)
            .setPositiveButton(R.string.button_allow) { _, _ -> request.proceed() }
            .setNegativeButton(R.string.button_deny) { _, _ -> request.cancel() }
            .setCancelable(false)
            .setMessage(messageResId)
            .show()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        // NOTE: delegate the permission handling to generated function
        onRequestPermissionsResult(requestCode, grantResults)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_TAKE_PHOTO && resultCode == Activity.RESULT_OK) {
            if(currentPath.isNotBlank()) {
                list[currentPos].path = currentPath
                adapter?.notifyItemChanged(currentPos)
            }
        } else if (requestCode == REQUEST_TAKE_PHOTO && resultCode == Activity.RESULT_CANCELED) {
            list[currentPos].path = ""
            adapter?.notifyItemChanged(currentPos)

        }
    }
}