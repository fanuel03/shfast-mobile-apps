package com.bcaf.shfast.ui.showroom

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.bcaf.shfast.R
import com.bcaf.shfast.persistence.api.Status
import com.bcaf.shfast.ui.LoginActivity
import com.bcaf.shfast.util.SessionManager
import com.bcaf.shfast.viewmodel.showroom.ShowroomSettingViewModel
import com.bumptech.glide.Glide
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.firebase.crashlytics.FirebaseCrashlytics
import kotlinx.android.synthetic.main.activity_setting.*
import org.koin.android.viewmodel.ext.android.viewModel


class ShowroomSettingActivity: AppCompatActivity() {
    private val viewModel: ShowroomSettingViewModel by viewModel()

    companion object {
        @JvmStatic
        fun getIntent(context: Context): Intent {
            return Intent(context, ShowroomSettingActivity::class.java)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_setting)
        supportActionBar!!.title = "Pengaturan"
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)

        initEvent()
        observe()
    }

    private fun initEvent() {
        Glide.with(this).load(R.drawable.loading).into(imgLoading)

        btnLogout.setOnClickListener {
            MaterialAlertDialogBuilder(this)
                .setTitle("Konfirmasi")
                .setMessage("Apakah anda ingin logout ?")
                .setPositiveButton("Ya") { _, _ ->
                    //loading_setting.visibility = View.VISIBLE
                    loading_setting.visibility = View.VISIBLE
                    viewModel.logout()
                }
                .setNegativeButton("Tidak") { dialog, _ ->
                    dialog.dismiss()
                }
                .show()
        }


    }

    private fun observe() {
        viewModel.logoutData.observe(this, Observer {
            loading_setting.visibility = View.GONE
            if (it.status == Status.SUCCESS) {
                SessionManager.flush(this)
                startActivity(LoginActivity.getIntent(this).apply {
                    flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                })
            } else {
                FirebaseCrashlytics.getInstance().log("Error Logout: ${it.message.toString()}")
                Toast.makeText(this, "Terjadi kesalahan ketika mengirim data", Toast.LENGTH_LONG).show()
            }
        })
    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                finish()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
}