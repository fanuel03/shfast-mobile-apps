package com.bcaf.shfast.ui.showroom

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.bcaf.shfast.R
import com.bcaf.shfast.ext.toRupiah
import com.bcaf.shfast.persistence.api.Status
import com.bcaf.shfast.persistence.response.showroom.history.HistorySHFBody
import com.bcaf.shfast.viewmodel.showroom.ShowroomHistoryDetailViewModel
import com.bumptech.glide.Glide
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import kotlinx.android.synthetic.main.activity_detail_pengajuan_showroom.*
import org.koin.android.viewmodel.ext.android.viewModel
import java.text.SimpleDateFormat
import java.util.*

class ShowroomDetailPengajuanActivity: AppCompatActivity() {

    private val viewModel: ShowroomHistoryDetailViewModel by viewModel()

    companion object {
        const val DETAIL_REQUEST_ID = 3378
        private const val EXTRA_ID = "EXTRA_ID"
        @JvmStatic
        fun getIntent(context: Context, idPengajuan: String): Intent {
            return Intent(context, ShowroomDetailPengajuanActivity::class.java).apply {
                putExtra(EXTRA_ID, idPengajuan)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_detail_pengajuan_showroom)
        supportActionBar!!.title = "Detail Pengajuan"
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        Glide.with(this).load(R.drawable.loading).into(imgLoading)

        val id = intent.getStringExtra(EXTRA_ID) as String
        viewModel.setIdPengajuan(id)

        initEvent()
        observe()
        viewModel.fetchDetail(id)
    }

    private fun initEvent() {
        loading_detail.visibility = View.VISIBLE
        btnCancel.visibility = View.GONE

    }

    private fun initCancelRequest(isInBucket: Boolean = false) {
        if (!isInBucket) {
            btnCancel.setOnClickListener {
                MaterialAlertDialogBuilder(this)
                    .setTitle("Konfirmasi")
                    .setMessage("Apakah anda ingin membatalkan pengajuan ini ?")
                    .setPositiveButton("OK") { _, _ ->
                        loading_detail.visibility = View.VISIBLE
                        viewModel.cancelRequest()
                    }
                    .setNegativeButton("Tidak") { dialog, _ ->
                        dialog.dismiss()
                    }
                    .show()
            }
        } else {
            btnCancel.setOnClickListener {
                MaterialAlertDialogBuilder(this)
                    .setTitle("Pengajuan")
                    .setMessage("Pengajuan tidak bisa dibatalkan karena sudah masuk ke komite")
                    .setPositiveButton("OK") { dialog, _ ->
                       dialog.dismiss()
                    }
                    .show()
            }
        }
    }

    private fun observe() {
        viewModel.detailData.observe(this, Observer {
            loading_detail.visibility = View.GONE
            if (it.status == Status.SUCCESS) {
                if (it.data is HistorySHFBody) {
                    val response = it.data.historyStatus!!
                    val data = response.data!!

                    val cal: Calendar = Calendar.getInstance()
                    cal.time =  SimpleDateFormat("yyyy-MM-dd").parse(data.tanggalPenggunaan)

                    val monthDate = SimpleDateFormat("MMMM")
                    val monthName: String = monthDate.format(cal.time)

                    // Split status karena status dapat berupa Progress-Komite-DVH
                    val splitStatus = data.status?.split("-")
                    txtStatus.text = splitStatus?.get(0) ?: "-"
                    txtTanggal.text = "${cal.get(Calendar.DATE)} $monthName ${cal.get(Calendar.YEAR)}"

                    txtOtr.text = data.otrTmp!!.toLong().toRupiah()

                    viewModel.setOtr(data.otrTmp.toString())
                    txtNopol.text = data.noPlat
                    txtTahun.text = data.tahun
                    txtTipe.text = data.tipe

                    when {
                        data.status!!.toLowerCase(Locale.ROOT) == "rejected" -> {
                            var reject = ""
                            reject = if (data.keteranganPic == "Penggunaan Baru") {
                                data.statusRekomendasi?.last { status-> status.keterangan != null }?.keterangan ?: "Reject by komite"
                            } else {
                                data.keteranganPic.toString()
                            }
                            txtNote.text = "Ditolak karena: $reject"
                        }
                        data.status.toLowerCase(Locale.ROOT) == "approved" -> {
                            txtNote.text = "Disetujui dengan OTR sebesar ${data.otrRekomen!!.toLong().toRupiah()}"
                            containerRecOtr.visibility = View.VISIBLE
                            txtRecOtr.text = data.otrRekomen.toLong().toRupiah()
                        }
                        data.status.toLowerCase(Locale.ROOT) == "canceled" -> {
                            txtNote.text = "Pengajuan telah Anda batalkan"
                        }

                        else -> {
                            txtNote.text = "Pengajuan sedang dalam proses"
                            btnCancel.visibility = View.VISIBLE
                            if (splitStatus?.size!! == 1) {
                                initCancelRequest(isInBucket = false)
                            } else {
                                initCancelRequest(true)
                            }
                        }
                    }

                } else {
                    Log.e("DETAIL", "Bukan History Body")
                    toastError("Terjadi kesalahan saat mengambil data")
                }

            } else {
                Log.e("DETAIL", it.message.toString())
                toastError("Terjadi kesalahan saat mengambil data")
            }
        })

        viewModel.cancelData.observe(this, Observer {
            loading_detail.visibility = View.GONE
            if (it.status == Status.SUCCESS) {
                MaterialAlertDialogBuilder(this)
                    .setTitle("Pembatalan Berhasil")
                    .setMessage("Tekan OK untuk kembali ke Halaman Riwayat")
                    .setPositiveButton("OK") { _, _ ->
                        setResult(Activity.RESULT_OK)
                        finish()
                    }.setCancelable(false)
                    .show()

            } else {
                toastError("Terjadi kesalahan saat membatalkan pengajuan")
            }
        })
    }

    private fun toastError(msg: String) {
        Toast.makeText(this.applicationContext, msg, Toast.LENGTH_LONG).show()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }




}