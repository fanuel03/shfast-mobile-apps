package com.bcaf.shfast.ui

import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.content.pm.Signature
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.bcaf.shfast.BuildConfig
import com.bcaf.shfast.R
import com.bcaf.shfast.persistence.api.Status
import com.bcaf.shfast.persistence.response.showroom.ShowroomDashboardBody
import com.bcaf.shfast.ui.komite.KomiteDashboardActivity
import com.bcaf.shfast.ui.showroom.ShowroomDashboardActivity
import com.bcaf.shfast.util.SHFConstant
import com.bcaf.shfast.util.SHFUtil
import com.bcaf.shfast.util.SessionManager
import com.bcaf.shfast.viewmodel.SplashScreenViewModel
import com.scottyab.rootbeer.RootBeer
import org.koin.android.viewmodel.ext.android.viewModel


class SplashScreenActivity: AppCompatActivity() {
    private val viewModel: SplashScreenViewModel by viewModel()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //code untuk fullscreen
        supportActionBar!!.hide()
        setContentView(R.layout.activity_splash_screen)

        val sigs: Array<Signature> = applicationContext.packageManager.getPackageInfo(applicationContext.packageName, PackageManager.GET_SIGNATURES).signatures
        for (sig in sigs) {
            //Toast.makeText(this, "Signature hashcode : " + sig.hashCode(), Toast.LENGTH_LONG).show()
           // Log.d("MyApp", "Signature hashcode : " + sig.hashCode())
        }

        try {
            val pInfo: PackageInfo =
                applicationContext.packageManager.getPackageInfo(applicationContext.packageName, 0)
            val version = pInfo.versionName
            if (BuildConfig.DEBUG) Toast.makeText(this, "Version: $version", Toast.LENGTH_LONG).show()
        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
        }


        val rootBeer = RootBeer(this)
        if (!rootBeer.isRooted) {
            observe()
            checkToken()
        } else {
            kickOut()
        }
    }

    private fun observe() {
        viewModel.checkTokenData.observe(this, Observer {
            if (it.status == Status.SUCCESS) {
                if (viewModel.getRole()!! == SHFConstant.ROLE_USER) {

                    if (it.data is ShowroomDashboardBody) {
                        SessionManager.setTanggalExpired(this, it.data.masaBerlaku!!)
                    }

                    startActivity(ShowroomDashboardActivity.getIntent(this).apply {
                        flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                    })
                } else if (viewModel.getRole()!! != SHFConstant.ROLE_CMO) {

                    startActivity(KomiteDashboardActivity.getIntent(this).apply {
                        flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                    })
                }
            } else {
                Log.e("SPLASH", it.message.toString())
                if (it.isTokenInvalid()) {
                    SHFUtil.tokenExpired(this)
                } else {
                    startActivity(LoginActivity.getIntent(this).apply {
                        flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                    })
                }
            }
        })
    }

    override fun onBackPressed() {
        // Do nothing
    }

    private fun checkToken() {
        viewModel.searchSession()
    }

    private fun kickOut() {
        val alert = AlertDialog.Builder(this)
        alert.setTitle("Can't Run On This Device")
            .setIcon(R.drawable.ic_warning)
            .setMessage("Aplikasi tidak dapat dijalankan pada Device yang melakukan Root.")
            .setPositiveButton("OK", DialogInterface.OnClickListener { _, _ ->
                finish()
            })
            .setCancelable(false)
            .show()
        Log.d("This Device Is Rooted ","True")
    }





}