package com.bcaf.shfast.di.module

import com.bcaf.shfast.repository.KomiteRepository
import com.bcaf.shfast.repository.LoginRepository
import com.bcaf.shfast.repository.ShowroomRepository
import com.bcaf.shfast.repository.SplashScreenRepository
import org.koin.dsl.module

val repoModule = module {
    single {
        SplashScreenRepository(get(), get())
    }
    single { LoginRepository(get(), get()) }
    single { ShowroomRepository(get(), get()) }
    single { KomiteRepository(get(), get()) }
}
