package com.bcaf.shfast.di.module

import com.bcaf.shfast.viewmodel.LoginViewModel
import com.bcaf.shfast.viewmodel.SplashScreenViewModel
import com.bcaf.shfast.viewmodel.komite.*
import com.bcaf.shfast.viewmodel.showroom.*
import org.koin.android.ext.koin.androidContext
    import org.koin.android.viewmodel.dsl.viewModel
    import org.koin.dsl.module

val viewModelModule = module {
    viewModel {
        SplashScreenViewModel(get(), androidContext())
    }
    viewModel {
        LoginViewModel(get(), androidContext())
    }
    viewModel {
        ShowroomDashboardViewModel(get(), androidContext())
    }
    viewModel {
        ShowroomPengajuanViewModel(get(), androidContext())
    }
    viewModel {
        ShowroomHistoryViewModel(get(), androidContext())
    }

    viewModel {
        ShowroomHistoryDetailViewModel(get(), androidContext())
    }

    viewModel {
        ShowroomContractViewModel(get(), androidContext())
    }

    viewModel {
        ShowroomContractDetailViewModel(get(), androidContext())
    }

    viewModel {
        ShowroomSettingViewModel(get(), androidContext())
    }

    // Viewmodel Komite

    viewModel {
        KomiteBucketApprovalViewModel(get(), androidContext())
    }

    viewModel {
        KomiteDetailApprovalViewModel(get(), androidContext())
    }

    viewModel {
        KomiteRiwayatApprovalViewModel(get(), androidContext())
    }

    viewModel {
        KomiteDetailRiwayatApprovalViewModel(get(), androidContext())
    }

    viewModel {
        KomiteSettingViewModel(get(), androidContext())
    }

    viewModel {
        ShowroomChangePasswordViewModel(get(), androidContext())
    }
}
