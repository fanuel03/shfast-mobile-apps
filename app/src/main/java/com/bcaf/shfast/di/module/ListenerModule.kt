package com.bcaf.shfast.di.module

import com.bcaf.shfast.viewmodel.showroom.ShowroomListener
import org.koin.dsl.module

val listenerModule = module {
    single {
        ShowroomListener()
    }
}