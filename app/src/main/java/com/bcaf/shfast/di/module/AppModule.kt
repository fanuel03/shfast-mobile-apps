package com.bcaf.shfast.di.module

import android.content.Context
import com.bcaf.shfast.data.ApiHelper
import com.bcaf.shfast.data.ApiHelperImpl
import com.bcaf.shfast.data.ApiService
import com.bcaf.shfast.data.komite.KomiteHelper
import com.bcaf.shfast.data.komite.KomiteHelperImpl
import com.bcaf.shfast.data.komite.KomiteService
import com.bcaf.shfast.data.showroom.ShowroomHelper
import com.bcaf.shfast.data.showroom.ShowroomHelperImpl
import com.bcaf.shfast.data.showroom.ShowroomService
import com.bcaf.shfast.persistence.api.ResponseHandler
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import okhttp3.CertificatePinner
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.android.BuildConfig
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

val appModule = module {
    single { provideOkHttpClient() }
    //single { provideRetrofit(get(), "http://147.139.171.241:443") }
    single { provideRetrofit(get()) }
    single { provideApiService(get()) }
    single { provideShowroomService(get()) }
    single { provideResponseHandler(androidContext()) }
    single<ApiHelper> {
        return@single ApiHelperImpl(get())
    }
    single<ShowroomHelper> {
        return@single ShowroomHelperImpl(get())
    }
    single { provideKomiteService(get()) }
    single<KomiteHelper> {
        return@single KomiteHelperImpl(get())
    }
}
    private fun provideResponseHandler(context: Context) = ResponseHandler(context)

    private fun provideOkHttpClient(): OkHttpClient {

        val certificatePinner = CertificatePinner.Builder()
            .add(
                "shfast.bcafinance.co.id",
                "sha256/sJzidPmP8ALI/5/m6PWQ329xCFYgzzW5P5ANuqrEEXU=",
                "sha256/5kJvNEMw0KjrCAu7eXY5HZdvyCS13BbA0VJG1RSP91w=",
                "sha256/r/mIkG3eEpVdm+u/ko/cwxzOMo1bk4TyHIlByibiA5E="
            )
            .build()

        if (BuildConfig.DEBUG) {
            val loggingInterceptor = HttpLoggingInterceptor()
            loggingInterceptor.apply {
                loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
            }
            return OkHttpClient.Builder()
                .readTimeout(30, TimeUnit.SECONDS)
                .connectTimeout(30, TimeUnit.SECONDS).addInterceptor(loggingInterceptor)
                .build()
        } else {
            return OkHttpClient.Builder()
                .readTimeout(30, TimeUnit.SECONDS)
                .connectTimeout(30, TimeUnit.SECONDS)
                .certificatePinner(certificatePinner)
                .build()
        }

    }

    private fun provideRetrofit(
        okHttpClient: OkHttpClient
    ): Retrofit {
        val BASE_URL: String = com.bcaf.shfast.BuildConfig.BASE_URL
        val gson: Gson = GsonBuilder()
            .setLenient()
            .create()


        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .client(okHttpClient)
            .build()
    }

    private fun provideApiService(retrofit: Retrofit): ApiService = retrofit.create(ApiService::class.java)
    private fun provideApiHelper(apiHelper: ApiHelperImpl): ApiHelper = apiHelper

    private fun provideShowroomService(retrofit: Retrofit): ShowroomService = retrofit.create(ShowroomService::class.java)
    private fun provideShowroomHelper(showroomHelper: ShowroomHelperImpl): ShowroomHelper = showroomHelper

    private fun provideKomiteService(retrofit: Retrofit): KomiteService = retrofit.create(KomiteService::class.java)
    private fun provideKomiteHelper(komiteHelper: KomiteHelperImpl): KomiteHelper = komiteHelper

